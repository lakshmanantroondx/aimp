//server.js

import express from 'express';
import 'babel-polyfill';
import cors from 'cors';
const bodyParser = require("body-parser");
const socketIo = require("socket.io");
import dbQuery from './app/db/dev/dbQuery';
import districtRoute from "./app/routes/master/districtRoute";
import userRoute from "./app/routes/authentication/userRoute";
import constituencyRoute from "./app/routes/master/constituencyRoute";
import casteRoute from "./app/routes/master/casteRoute";
import grievancecategoryRoute from "./app/routes/master/grievancecategoryRoute";
import grievancesubcategoryRoute from "./app/routes/master/grievancesubcategoryRoute";
import religionRoute from "./app/routes/master/religionRoute";
import wardRoute from "./app/routes/master/wardRoute";
import surveyQuestionRoute from "./app/routes/survey/surveyQuestionRoute";
import surveyUserRoute from "./app/routes/survey/surveyUserRoute";
import surveyUserGroupRoute from "./app/routes/survey/surveyUserGroupRoute";
import usermanagementRoute from "./app/routes/settings/usermanagementRoute";
import rolemanagementRoute from "./app/routes/settings/rolemanagementRoute";

const http = require("http");
import dotenv from 'dotenv';
dotenv.config();
const app = express();
const expressSwagger = require('express-swagger-generator')(app);

let options = {
  swaggerDefinition: {
      info: {
          description: 'Server Side Route',
          title: 'TroonDx',
          version: '1.0.0',
      },
	  // host: '1.2.3.4',
    // host : 'test.com',
      host : 'localhost:5000',
      basePath: '/api/v1',
      produces: [
          "application/json",
          "application/xml"
      ],
      //schemes: ['http', 'https'],
      securityDefinitions: {
          JWT: {
              type: 'apiKey',
              in: 'header',
              name: 'Authorization',
              description: "",
          }
      }
  },
  basedir: __dirname, //app absolute path
  files: ['./app/routes/**/*.js'] //Path to the API handle folder
};

expressSwagger(options)
import PartyMemberRoute from './app/routes/election/PartyMemberRoute'; import BoothHistoryRoute from './app/routes/election/BoothHistoryRoute'; import VehicleManagementRoute from './app/routes/election/VehicleManagementRoute'; import ElectionPeriodRoute from './app/routes/election/ElectionPeriodRoute'; import PartyEventsRoute from './app/routes/election/PartyEventsRoute'; import CandidateRoute from './app/routes/election/CandidateRoute'; import PartyRoute from './app/routes/election/PartyRoute'; import VoterRoute from './app/routes/election/VoterRoute'; import BoothExecutiveRegionRoute from './app/routes/election/BoothExecutiveRegionRoute'; import BoothRoute from './app/routes/election/BoothRoute'; import citizenAppRoute from './app/routes/citizen/citizenAppRoute'; 
const server = http.createServer(app);
const io = socketIo(server);

global.__basedir = __dirname;

// Add middleware for parsing URL encoded bodies (which are usually sent by browser)
app.use(cors());

var clients = [];
io.sockets.on('connection', function (socket) {
  console.log("Connection")
  io.sockets.emit('getroom', "ekycVCIP");
  socket.on('storeClientInfo', async function (data) {
    console.log("new User emitter")
    var clientInfo = new Object();
    var id       = data.id;
    var orgid    = data.orgid;
    var userType = data.userType;
    var socketid = socket.id;
    console.log("Sockets info", socketid, userType, orgid, id)
    //io.to(socketid).emit('Room Change',"testing")
    if(userType=="participants"){
      const findUserQuery = 'SELECT * FROM participants WHERE id=$1 and orgid=$2';
      const updateUser = 'UPDATE participants SET socketid=$1  WHERE id=$2 and orgid=$3 returning *';
      try {
          const { rows } = await dbQuery.query(findUserQuery, [id,orgid]);
          const dbResponse = rows[0];

          if (!dbResponse) {
              var errorMessage = 'Participant not found';
              console.log("errorMessage",errorMessage);
          }
          const values = [
              socketid,
              id,
              orgid
          ];
          const response = await dbQuery.query(updateUser, values);
          const dbResult = response.rows[0];
          console.log("Updated");
      } catch (error) {
          var errorMessage = 'Operation was not successful';
      }
    }
  });
  socket.on('disconnect', function (data) {
    for (var i = 0, len = clients.length; i < len; ++i) {
      var c = clients[i];
      if (c.clientId == socket.id) {
        clients.splice(i, 1);
        break;
      }
    }
  });
});

// Make io accessible to our router
app.use(function(req,res,next){
  req.io = io;
  next();
});

// Add middleware for parsing JSON and urlencoded data and populating `req.body`
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api/v1', districtRoute);
app.use('/api/v1', userRoute);
app.use('/api/v1', constituencyRoute);
app.use('/api/v1', casteRoute);
app.use('/api/v1', grievancecategoryRoute);
app.use('/api/v1', grievancesubcategoryRoute);
app.use('/api/v1', religionRoute);
app.use('/api/v1', wardRoute);
app.use('/api/v1', surveyQuestionRoute);
app.use('/api/v1', surveyUserRoute);
app.use('/api/v1',surveyUserGroupRoute);
app.use('/uploads', express.static('uploads'));
app.use('/api/v1',usermanagementRoute);
app.use('/api/v1',rolemanagementRoute);
// //Getting UI From Dist Folder For Render Change By JJ
// app.get('/*', function(req, res) {
//   res.sendFile(path.join(__dirname, './dist/index.html'), function(err) {
//     if (err) {
//       res.status(500).send(err)
//     }
//   })
// })

var AppRoutes;
app.use('/api/v1', PartyMemberRoute); app.use('/api/v1', BoothHistoryRoute); app.use('/api/v1', VehicleManagementRoute); app.use('/api/v1', ElectionPeriodRoute); app.use('/api/v1', PartyEventsRoute); app.use('/api/v1', CandidateRoute); app.use('/api/v1', PartyRoute); app.use('/api/v1', VoterRoute); app.use('/api/v1', BoothExecutiveRegionRoute); app.use('/api/v1', BoothRoute); app.use('/api/v1', citizenAppRoute); 
app.get("/", (req, res) => {
  res.json({ message: "AIMP API Lists" });
});

server.listen(process.env.PORT).on('listening', () => {
  console.log(`🚀 AIMP API Lists are live on ${process.env.PORT}`);
});

export default app;
