//db/dev/dbConnection.js
import pool from './pool';

pool.on('connect', () => {
    console.log('connected to the db');
});


const createUsers = () => {
    const userQuery = `CREATE TABLE IF NOT EXISTS users (
        id SERIAL PRIMARY KEY,
        email VARCHAR ,
        mobileNo VARCHAR UNIQUE,
        isverified VARCHAR,
        lastlogin DATE DEFAULT CURRENT_DATE,
        otp VARCHAR,
        status VARCHAR,
        deleted INTEGER DEFAULT 0
      )`;

    pool.query(userQuery)
        .then((res) => {
            console.log("res", res);
        })
        .catch((err) => {
            console.log(err);
        });
};

const createUserProfile = () => {
    const profileQuery = `CREATE TABLE IF NOT EXISTS userprofile (
        id SERIAL PRIMARY KEY,
        userid INTEGER REFERENCES users(id),
        firstname VARCHAR,
        lastname VARCHAR,
        gender VARCHAR,
        emergencyno VARCHAR,
        createdDate DATE DEFAULT CURRENT_DATE,
        modifiedDate DATE DEFAULT CURRENT_DATE,
        deleted INTEGER DEFAULT 0
      )`;

    pool.query(profileQuery)
        .then((res) => {
            console.log("res", res);
        })
        .catch((err) => {
            console.log(err);
        });
};

/**
 * Drop Table
 */
const dropuserprofile = () => {
    const dropuserprofile = 'DROP TABLE IF EXISTS userprofile';
    pool.query(dropuserprofile)
        .then((res) => {
            console.log("userprofile dropped", res);
        })
        .catch((err) => {
            console.log(err);
        });
};


const dropusers = () => {
    const dropusers = 'DROP TABLE IF EXISTS users';
    pool.query(dropusers)
        .then((res) => {
            console.log("uesrs dropped", res);
        })
        .catch((err) => {
            console.log(err);
        });
};

/**
 * Create All Tables
 */
const createAllTables = () => {
    createUsers();
    createUserProfile();
};

/**
 * Drop All Tables
 */
const dropAllTables = () => {
    dropuserprofile();
    dropusers();
};

pool.on('remove', () => {
    console.log('client removed');
    process.exit(0);
});

export {
    createAllTables,
    dropAllTables,
};

require('make-runnable');