//db/dev/dbQuery.js

import connection from './pool';

export default {
    /**
     * DB Query
     * @param {object} req
     * @param {object} res
     * @returns {object} object
     */
    
    query(quertText, params) {
        return new Promise((resolve, reject) => {
            connection.query(quertText, params)
                .then((res) => {
                    // debugg postgres error
                    //console.log("res value is",res)
                    resolve(res);
                })
                .catch((err) => {
                    //console.log("error in dbquery",err)
                    reject(err);
                });
        });
    },
};