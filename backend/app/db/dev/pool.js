//db/dev/pool.js

import { Pool } from 'pg';

import dotenv from 'dotenv';

dotenv.config();

// const databaseConfig = { connectionString: process.env.DATABASE_URL };
// const pool = new Pool(databaseConfig);

var mysql      = require('mysql');
// var connection = mysql.createConnection({
//   host     : '129.151.44.205',
//   port:3306,
//   user     : 'root',
//   password : 'NDusr@1234',
//   database : 'ai_demo_31_08_2021'
// });

var connection = mysql.createConnection({
  host     : 'localhost',
  port:3306,
  user     : 'lakshman',
  password : 'Password@1234',
  database : 'aimp_sep_2021'
});

connection.connect(function(err) {
  if (err) {
      console.error('Error connecting: ' + err.stack);
      return;
  }
  console.log('Connected as id ' + connection.threadId);
});

export default connection;