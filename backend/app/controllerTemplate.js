"import moment from 'moment';"
"import dbQuery from '../../db/dev/dbQuery';"

"import {isEmpty, empty } from '../../helpers/validations';"

"import {errorMessage,successMessage,status,} from '../../helpers/status';"

"/**"
"   * Create ${i}"
"   * @param {object} req"
"   * @param {object} res"
"   * @returns {object} reflection object"
"   */"

"const create${i} = async (req, res) => {"
"    console.log('testing', req.body)"
"    const { } = req.body;"
""
"    const created_on = moment(new Date());"
""
"    const createQuery = 'insert into ${i} () values() returning *';"
"    const values = ["
"    ];"
""
"    try {"
"        const { rows } = await dbQuery.query(createQuery, values);"
"        const dbResponse = rows[0];"
"        return res.status(status.created).send(dbResponse);"
"    } catch (error) {"
"        if (error.routine === '_bt_check_unique') {"
"            errorMessage.error = 'Unable to create';"
"            return res.status(status.conflict).send(errorMessage);"
"        }"
"    }"
"};"

"/**"
" * Update ${i}"
" * @param {object} req "
" * @param {object} res "
" * @returns {object} updated ${i}"
" */"

"const update${i} = async (req, res) => {"
"   const {  } = req.params;"
"   const {  } = req.body;"
"   const {  } = req.user;"

"   const findQuery = 'select * from ${i} where ----';"

"   const updateQuery = 'update ${i} set ---- where ---- ';"

"   try {"
"       const { rows } = await dbQuery.query(findQuery, []);"
"       const dbResponse = rows[0];"
"       if (!dbResponse) {"
"            errorMessage.error = 'Not found';"
"            return res.status(status.notfound).send(errorMessage);"
"       }"
"       const values = ["
            ""
"        ];"
"       const response = await dbQuery.query(updateQuery, values);"
"       const dbResult = response.rows[0];"
"       return res.status(status.success).send(dbResult);"
"   } catch (error) {"
"       errorMessage.error = 'Operation was not successful';"
"       return res.status(status.error).send(errorMessage);"
"   }"
"};"

"/**"
"   * Delete ${i}"
"   * @param {object} req "
"   * @param {object} res "
"   * @returns {void} "
"   */"

"const delete${i} = async (req, res) => {"
"    const {  } = req.params;"
"    const {  } = req.user;"
"    const deleteQuery = 'delete from ${i} where ---- ';"
"    try {"
"        const { rows } = await dbQuery.query(deleteQuery, []);"
"        const dbResponse = rows[0];"
"        if (!dbResponse) {"
"            errorMessage.error = 'You have no data with this parm ';"
"            return res.status(status.notfound).send(errorMessage);"
"        }"
"        successMessage.data = {};"
"        successMessage.data.message = 'Deleted successfully';"
"        return res.status(status.success).send(successMessage);"
"    } catch (error) {"
"        return res.status(status.error).send(error);"
"    }"

"};"


"/**"
"   * Get All "
"   * @param {object} req "
"   * @param {object} res "
"   * @returns {object} "
"   */"
"const getAll${i} = async (req, res) => {"
"    const { } = req.user.parms;"
"    const getAllQuery = 'select * from ${i} where --- ';"
"    try {"
"        const { rows } = await dbQuery.query(getAllQuery, []);"
"        const dbResponse = rows;"

"        if (dbResponse[0] === undefined) {"
"            errorMessage.error = 'No data found';"
"            return res.status(status.notfound).send(errorMessage);"
"        }"
"        return res.status(status.success).send(dbResponse);"
"    } catch (error) {"
"        errorMessage.error = 'An error Occured';"
"        return res.status(status.error).send(errorMessage);"
"    }"
"};"

"/**"
"   * Get get${i}ById"
"   * @param {object} req "
"   * @param {object} res "
"   * @returns {object} "
"   */"
"const get${i}ById = async (req, res) => {"
"    const { } = req.user.parms;"
"    const { } = req.params;"

"    var getQuery = 'select * from ${i} where ---- ';"

"    try {"
"        const { rows } = await dbQuery.query(getQuery, []);"
"        const dbResponse = rows;"
"        if (dbResponse[0] === undefined) {"
"            errorMessage.error = 'No data found';"
"            return res.status(status.notfound).send(errorMessage);"
"        }"
"        return res.status(status.success).send(dbResponse);"
"    } catch (error) {"
"        errorMessage.error = 'An error Occured';"
"        return res.status(status.error).send(errorMessage);"
"    }"
"};"

"export {"
"    create${i},"
"    getAll${i},"
"    update${i},"
"    delete${i},"
"    get${i}ById"
"};"