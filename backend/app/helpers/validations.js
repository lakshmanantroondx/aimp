//app/helpers/validation.js

import jwt from 'jsonwebtoken';
const bcrypt = require("bcryptjs");
const fs = require("fs");
var csv = require("fast-csv");
import moment from 'moment';
import dbQuery from '../db/dev/dbQuery';
import { errorMessage, successMessage, status, } from '../helpers/status';

import dotenv from 'dotenv';
dotenv.config();

/**
   * isValidEmail helper method
   * @param {string} email
   * @returns {Boolean} True or False
   */
const isValidEmail = (email) => {
    const regEx = /\S+@\S+\.\S+/;
    return regEx.test(email);
};

/**
   * validatePassword helper method
   * @param {string} password
   * @returns {Boolean} True or False
   */
const validatePassword = (password) => {
    if (password.length <= 5 || password === '') {
        return false;
    } return true;
};
/**
   * isEmpty helper method
   * @param {string, integer} input
   * @returns {Boolean} True or False
   */
const isEmpty = (input) => {
    if (input === undefined || input === '') {
        return true;
    }
    if (input.replace(/\s/g, '').length) {
        return false;
    } return true;
};

/**
   * empty helper method
   * @param {string, integer} input
   * @returns {Boolean} True or False
   */
const empty = (input) => {
    if (input === undefined || input === '') {
        return true;
    }
};

/**
   * Generate Token
   * @param {string, integer} input
   * @returns {Boolean} True or False
   */
const generateToken = async (parms) => {
    const token = await jwt.sign({
        parms
    }, process.env.secret, { expiresIn: "3d" });
    return token;
};

const hashPassword = async (password) => {
    const salt = await bcrypt.genSalt(10);
    var password = await bcrypt.hash(password, salt);
    return password;
};

const comparePassword = async (password, dbPassword) => {
    const status = await bcrypt.compare(password, dbPassword)
    return status;
}

async function importCsvData2MySQL(filePath,ty_pe,req,res){
    let stream = fs.createReadStream(filePath);
    let csvData = [];
    let csvStream = csv
        .parse()
        .on("data", function (data) {
            csvData.push(data);
        })
        .on("end", async function () {
            // Remove Header ROW
            csvData.shift();
            const createdDate = moment(new Date());
            const modifiedDate = moment(new Date());
            if (ty_pe == 'tablename'){
                try {
                    console.log("req.user",req)
                    const insertQuery = `INSERT INTO
                                table (itemtype, createdDate,modifiedDate)
                                VALUES($1, $2, $3)
                                returning *`;
                    var totalData = csvData.length;
                    var loop = 0;
                    csvData.forEach(async (row,index) => {
                        var rows = [];
                        rows[0] = row[0];
                        rows[1] = createdDate;
                        rows[2] = modifiedDate;
                        console.log("Rows value is",rows)
                        const { obs } = await dbQuery.query(insertQuery, rows);
                        loop = loop + 1;
                        if(totalData == loop){
                            console.log("Finally")
                            return res.status(status.created).send("Bulk Insert Successfull");
                        }
                    });
                } catch (error) {
                    if (error.routine === '_bt_check_unique') {
                        errorMessage.error = 'already exist';
                        return res.status(status.conflict).send(errorMessage);
                    }
                }

            }
            if (ty_pe == 'mastervalues'){
                try {
                    console.log("req.user",req)
                    const {itemtypeid} = req.body;
                    const insertQuery = `INSERT INTO
                                mastervalues (itemtypeid, itemvalue, createdDate,modifiedDate)
                                VALUES($1, $2, $3, $4)
                                returning *`;
                    var totalData = csvData.length;
                    var loop = 0;
                    csvData.forEach(async (row,index) => {
                        var rows1 = [];
                        rows1[0] = itemtypeid;
                        rows1[1] = row[0];
                        rows1[2] = createdDate;
                        rows1[3] = modifiedDate;
                        console.log("Rows value is",rows1)
                        const { rows } = await dbQuery.query(insertQuery, rows1);
                        loop = loop + 1;
                        if(totalData == loop){
                            console.log("Finally")
                            return res.status(status.created).send("Bulk Insert Successfull");
                        }
                    });
                } catch (error) {
                    if (error.routine === '_bt_check_unique') {
                        errorMessage.error = 'already exist';
                        return res.status(status.conflict).send(errorMessage);
                    }
                }
            }
            if (ty_pe == 'users'){
                try {
                    var isverified = 'False';
                    var statuses = 'Active';
                    var otp = '';
                    console.log("req.user",req)
                    const lastlogin = moment(new Date());
                    const insertQuery = `INSERT INTO
                                users (email, mobileno, isverified, lastlogin, otp, status)
                                VALUES($1, $2, $3, $4,$5,$6)
                                returning *`;
                    var totalData = csvData.length;
                    var loop = 0;
                    csvData.forEach(async (row,index) => {
                        var rows = [];
                        rows[0] = row[0];
                        rows[1] = row[1];
                        rows[2] = isverified;
                        rows[3] = lastlogin;
                        rows[4] = otp;
                        rows[5] = statuses;
                        console.log("Rows value is",rows)
                        const { obs } = await dbQuery.query(insertQuery, rows);
                        loop = loop + 1;
                        if(totalData == loop){
                            console.log("Finally")
                            return res.status(status.created).send("Bulk Insert Successfull");
                        }
                    });
                } catch (error) {
                    if (error.routine === '_bt_check_unique') {
                        errorMessage.error = 'already exist';
                        return res.status(status.conflict).send(errorMessage);
                    }
                }
            }
            if (ty_pe == 'location'){
                try {
                    const {state, country} = req.body;
                    var isverified = 'True';
                    var statuses = 'Active';
                    console.log("req.user",req)
                    const insertQuery = `INSERT INTO
                                location(city, state, taluk, country, pincode,township,createdDate,modifiedDate)
                                VALUES($1, $2, $3, $4, $5, $6, $7,$8)
                                returning *`;
                    var totalData = csvData.length;
                    var loop = 0;
                    csvData.forEach(async (row,index) => {
                        var rows = [];
                        rows[0] = row[0];
                        rows[1] = state;
                        rows[2] = row[1];
                        rows[3] = country;
                        rows[4] = row[2];
                        rows[5] = row[3];
                        rows[6] = createdDate;
                        rows[7] = modifiedDate;
                        console.log("Rows value is",rows)
                        const { obs } = await dbQuery.query(insertQuery, rows);
                        loop = loop + 1;
                        if(totalData == loop){
                            console.log("Finally")
                            return res.status(status.created).send("Bulk Insert Successfull");
                        }
                    });
                } catch (error) {
                    if (error.routine === '_bt_check_unique') {
                        errorMessage.error = 'already exist';
                        return res.status(status.conflict).send(errorMessage);
                    }
                }
            }
        fs.unlinkSync(filePath)
        });
    stream.pipe(csvStream);
}

export {
    isValidEmail,
    validatePassword,
    isEmpty,
    empty,
    generateToken,
    hashPassword,
    comparePassword,
    importCsvData2MySQL
};