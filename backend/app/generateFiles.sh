#!/bin/bash
# xargs touch < "file1.txt"

dir1="controllers"
dir2="routes"

folders='''
master
'''

files='''
district
'''

if [ ! -d "$dir1" ] 
then
    mkdir 'controllers'
    cd 'controllers'
    for i in ${folders}; do
        mkdir ${i}
    done
    cd ..
else
    cd 'controllers'
    for i in ${folders}; do
        mkdir ${i}
    done
    cd ..
fi

while read line; do
    for m in ${folders}; do
        cd 'controllers'
        cd ${m}
        for i in ${files}; do
            eval echo "$line" >> "${i}Controller.js"
        done
        cd ..
        cd ..
    done
done < "controllerTemplate.js"

if [ ! -d "$dir2" ] 
then
    mkdir 'routes'
    cd 'routes'
    for i in ${folders}; do
        mkdir ${i}
    done
    cd ..
else
    cd 'routes'
    for i in ${folders}; do
        mkdir ${i}
    done
    cd ..
fi

while read line; do
    for m in ${folders}; do
        cd 'routes'
        cd ${m}
        for i in ${files}; do
            eval echo "$line" >> "${i}Route.js"
        done
        cd ..
        cd ..
    done
done < "routeTemplate.js"

cd ..
for m in ${folders}; do
    for i in ${files}; do
        path1="import ${i}Route from './app/routes/${m}/${i}Route';"
        sed -i "" "/expressSwagger(options)/a\ 
        ${path1} " server.js

        path2="app.use('/api/v1', ${i}Route);"
        sed -i "" "/var AppRoutes;/a\ 
        ${path2} " server.js
    done
done

