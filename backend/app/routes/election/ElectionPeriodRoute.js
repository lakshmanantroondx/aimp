//app/routes/ElectionPeriodRoute.js

import express from 'express';

import {
createElectionPeriod,
getAllElectionPeriod,
deleteElectionPeriod,
getElectionPeriodById,
updateElectionPeriodbyID }
from '../../controllers/election/ElectionPeriodController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef CreateElectionPeriod
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createElectionPeriod
* @group ElectionPeriod API Layer - Operations on ElectionPeriod
* @param {CreateElectionPeriod.model} CreateElectionPeriod.body.required - Create ElectionPeriod
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/createElectionPeriod', createElectionPeriod);


/**
* This function comment is parsed by TroonDx
* @route GET /getAllElectionPeriod
* @group ElectionPeriod API Layer - Operations on ElectionPeriod
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAllElectionPeriod',  getAllElectionPeriod);



/**
* @typedef DeleteByIdElectionPeriod
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdElectionPeriod
* @group ElectionPeriod API Layer - Delete Operations on ElectionPeriod
* @param {DeleteByIdElectionPeriod.model} DeleteByIdElectionPeriod.body.required - DeleteById ElectionPeriod
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdElectionPeriod', deleteElectionPeriod);

/**
* @typedef GetElectionPeriodById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getElectionPeriodById
* @group ElectionPeriod API Layer - Get Operations on ElectionPeriod
* @param {GetElectionPeriodById.model} GetElectionPeriodById.body.required - GetElectionPeriodById ElectionPeriod
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/getElectionPeriodById',getElectionPeriodById);

router.post('/updateElectionPeriodbyID',updateElectionPeriodbyID);

export default router;
