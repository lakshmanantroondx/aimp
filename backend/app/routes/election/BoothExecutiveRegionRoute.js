//app/routes/BoothExecutiveRegionRoute.js

import express from 'express';

import {
createBoothExecutiveRegion,
getAllBoothExecutiveRegion,
updateBoothExecutiveRegion,
deleteBoothExecutiveRegion,
getBoothExecutiveRegionById }
from '../../controllers/election/BoothExecutiveRegionController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef CreateBoothExecutiveRegion
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createBoothExecutiveRegion
* @group BoothExecutiveRegion API Layer - Operations on BoothExecutiveRegion
* @param {CreateBoothExecutiveRegion.model} CreateBoothExecutiveRegion.body.required - Create BoothExecutiveRegion
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/createBoothExecutiveRegion', verifyAuth, createBoothExecutiveRegion);


/**
* This function comment is parsed by TroonDx
* @route GET /getAllBoothExecutiveRegion
* @group BoothExecutiveRegion API Layer - Operations on BoothExecutiveRegion
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAllBoothExecutiveRegion',verifyAuth,  getAllBoothExecutiveRegion);

/**
* @typedef UpdatebyIdBoothExecutiveRegion
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIdBoothExecutiveRegion
* @group BoothExecutiveRegion API Layer - Update Operations on BoothExecutiveRegion
* @param {UpdatebyIdBoothExecutiveRegion.model} UpdatebyIdBoothExecutiveRegion.body.required - UpdatebyID BoothExecutiveRegion
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIdBoothExecutiveRegion', verifyAuth, updateBoothExecutiveRegion);

/**
* @typedef DeleteByIdBoothExecutiveRegion
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdBoothExecutiveRegion
* @group BoothExecutiveRegion API Layer - Delete Operations on BoothExecutiveRegion
* @param {DeleteByIdBoothExecutiveRegion.model} DeleteByIdBoothExecutiveRegion.body.required - DeleteById BoothExecutiveRegion
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdBoothExecutiveRegion', verifyAuth, deleteBoothExecutiveRegion);

/**
* @typedef GetBoothExecutiveRegionById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getBoothExecutiveRegionById
* @group BoothExecutiveRegion API Layer - Get Operations on BoothExecutiveRegion
* @param {GetBoothExecutiveRegionById.model} GetBoothExecutiveRegionById.body.required - GetBoothExecutiveRegionById BoothExecutiveRegion
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getBoothExecutiveRegionById/:id', verifyAuth,getBoothExecutiveRegionById);

export default router;
