//app/routes/BoothRoute.js

import express from 'express';

import {
createBooth,
getAllBooth,
updateBooth,
deleteBooth,
getBoothById,
getBoothByfilter,
viewagentlist,
masterlist,
viewagentdetails,
totalvoters,
totalagents }
from '../../controllers/election/BoothController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef CreateBooth
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createBooth
* @group Booth API Layer - Operations on Booth
* @param {CreateBooth.model} CreateBooth.body.required - Create Booth
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/




/**
* This function comment is parsed by TroonDx
* @route GET /getAllBooth
* @group Booth API Layer - Operations on Booth
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAllBooth',  getAllBooth);


router.post('/updateByIdBooth', updateBooth);

/**
* @typedef DeleteByIdBooth
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdBooth
* @group Booth API Layer - Delete Operations on Booth
* @param {DeleteByIdBooth.model} DeleteByIdBooth.body.required - DeleteById Booth
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdBooth', deleteBooth);

/**
* @typedef GetBoothById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getBoothById
* @group Booth API Layer - Get Operations on Booth
* @param {GetBoothById.model} GetBoothById.body.required - GetBoothById Booth
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/

router.post('/createBooth', createBooth);

router.post('/getBoothById',getBoothById);

router.post('/getBoothByfilter',getBoothByfilter);

router.get('/viewagentlist',viewagentlist);

router.get('/masterlist',masterlist);

router.post('/viewagentdetails',viewagentdetails);

router.post('/totalvoters',totalvoters);

router.get('/totalagents',totalagents);

export default router;
