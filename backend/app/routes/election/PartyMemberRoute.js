//app/routes/PartyMemberRoute.js

import express from 'express';

import {
createPartyMember,
getAllPartyMember,
updatePartyMember,
deletePartyMember,
getPartyMemberById }
from '../../controllers/election/PartyMemberController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef CreatePartyMember
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createPartyMember
* @group PartyMember API Layer - Operations on PartyMember
* @param {CreatePartyMember.model} CreatePartyMember.body.required - Create PartyMember
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/createPartyMember', verifyAuth, createPartyMember);


/**
* This function comment is parsed by TroonDx
* @route GET /getAllPartyMember
* @group PartyMember API Layer - Operations on PartyMember
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAllPartyMember',verifyAuth,  getAllPartyMember);

/**
* @typedef UpdatebyIdPartyMember
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIdPartyMember
* @group PartyMember API Layer - Update Operations on PartyMember
* @param {UpdatebyIdPartyMember.model} UpdatebyIdPartyMember.body.required - UpdatebyID PartyMember
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIdPartyMember', verifyAuth, updatePartyMember);

/**
* @typedef DeleteByIdPartyMember
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdPartyMember
* @group PartyMember API Layer - Delete Operations on PartyMember
* @param {DeleteByIdPartyMember.model} DeleteByIdPartyMember.body.required - DeleteById PartyMember
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdPartyMember', verifyAuth, deletePartyMember);

/**
* @typedef GetPartyMemberById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getPartyMemberById
* @group PartyMember API Layer - Get Operations on PartyMember
* @param {GetPartyMemberById.model} GetPartyMemberById.body.required - GetPartyMemberById PartyMember
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getPartyMemberById/:id', verifyAuth,getPartyMemberById);

export default router;
