//app/routes/CandidateRoute.js

import express from 'express';

const multer = require('multer');

const candidateimgstorage = multer.diskStorage({
    destination: (req, file, cb) => {
       cb(null, __basedir + '/uploads/candidate/')
    },
    filename: (req, file, cb) => {
       cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname)
    }
 });

 const uploadcandidateimagestrge = multer({ storage : candidateimgstorage});


import {
createCandidate,
getAllCandidate,
updateCandidate,
deleteCandidate,
uploadcandidateimage,
getcandidatebyID}
from '../../controllers/election/CandidateController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef CreateCandidate
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createCandidate
* @group Candidate API Layer - Operations on Candidate
* @param {CreateCandidate.model} CreateCandidate.body.required - Create Candidate
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/createCandidate', createCandidate);


/**
* This function comment is parsed by TroonDx
* @route GET /getAllCandidate
* @group Candidate API Layer - Operations on Candidate
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAllCandidate',  getAllCandidate);

/**
* @typedef UpdatebyIdCandidate
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIdCandidate
* @group Candidate API Layer - Update Operations on Candidate
* @param {UpdatebyIdCandidate.model} UpdatebyIdCandidate.body.required - UpdatebyID Candidate
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIdCandidate', updateCandidate);

/**
* @typedef DeleteByIdCandidate
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdCandidate
* @group Candidate API Layer - Delete Operations on Candidate
* @param {DeleteByIdCandidate.model} DeleteByIdCandidate.body.required - DeleteById Candidate
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdCandidate', deleteCandidate);

router.post('/uploadcandidateimage', uploadcandidateimagestrge.single('candidateimage'), uploadcandidateimage); 

router.post('/getcandidatebyID',getcandidatebyID);

export default router;
