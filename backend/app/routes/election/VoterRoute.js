//app/routes/VoterRoute.js

import express from 'express';

import {
createVoter,
getAllVoter,
updateVoter,
deleteVoter,
getVoterById,
updatevoterpersonal,
updatevotercommunication,
updatevoterfinancial,
createvoterprediction,
getVoterAttendanceById }
from '../../controllers/election/VoterController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef CreateVoter
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createVoter
* @group Voter API Layer - Operations on Voter
* @param {CreateVoter.model} CreateVoter.body.required - Create Voter
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/createVoter', createVoter);


/**
* This function comment is parsed by TroonDx
* @route GET /getAllVoter
* @group Voter API Layer - Operations on Voter
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAllVoter/:district_id/:constituency_id/:ward_id/:booth_id',  getAllVoter);

/**
* @typedef UpdatebyIdVoter
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIdVoter
* @group Voter API Layer - Update Operations on Voter
* @param {UpdatebyIdVoter.model} UpdatebyIdVoter.body.required - UpdatebyID Voter
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIdVoter', updateVoter);

/**
* @typedef DeleteByIdVoter
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdVoter
* @group Voter API Layer - Delete Operations on Voter
* @param {DeleteByIdVoter.model} DeleteByIdVoter.body.required - DeleteById Voter
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdVoter', deleteVoter);

/**
* @typedef GetVoterById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getVoterById
* @group Voter API Layer - Get Operations on Voter
* @param {GetVoterById.model} GetVoterById.body.required - GetVoterById Voter
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/getVoterById',getVoterById);

router.get('/getVoterAttendanceById/:id',getVoterAttendanceById);

router.post('/updatevoterpersonal',updatevoterpersonal);

router.post('/updatevotercommunication',updatevotercommunication);

router.post('/updatevoterfinancial',updatevoterfinancial);

router.post('/createvoterprediction',createvoterprediction);
export default router;
