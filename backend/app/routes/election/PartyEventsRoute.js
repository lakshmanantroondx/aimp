//app/routes/PartyEventsRoute.js

import express from 'express';

import {
createPartyEvents,
getAllPartyEvents,
updatePartyEvents,
deletePartyEvents,
getPartyEventsById }
from '../../controllers/election/PartyEventsController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef CreatePartyEvents
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createPartyEvents
* @group PartyEvents API Layer - Operations on PartyEvents
* @param {CreatePartyEvents.model} CreatePartyEvents.body.required - Create PartyEvents
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/createPartyEvents', createPartyEvents);


/**
* This function comment is parsed by TroonDx
* @route GET /getAllPartyEvents
* @group PartyEvents API Layer - Operations on PartyEvents
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAllPartyEvents',  getAllPartyEvents);

/**
* @typedef UpdatebyIdPartyEvents
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIdPartyEvents
* @group PartyEvents API Layer - Update Operations on PartyEvents
* @param {UpdatebyIdPartyEvents.model} UpdatebyIdPartyEvents.body.required - UpdatebyID PartyEvents
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIdPartyEvents', updatePartyEvents);

/**
* @typedef DeleteByIdPartyEvents
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdPartyEvents
* @group PartyEvents API Layer - Delete Operations on PartyEvents
* @param {DeleteByIdPartyEvents.model} DeleteByIdPartyEvents.body.required - DeleteById PartyEvents
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdPartyEvents/:id', deletePartyEvents);

/**
* @typedef GetPartyEventsById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getPartyEventsById
* @group PartyEvents API Layer - Get Operations on PartyEvents
* @param {GetPartyEventsById.model} GetPartyEventsById.body.required - GetPartyEventsById PartyEvents
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/getPartyEventsById',getPartyEventsById);

export default router;
