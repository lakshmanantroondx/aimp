//app/routes/VehicleManagementRoute.js

import express from 'express';

import {
createVehicleManagement,
getAllVehicleManagement,
updateVehicleManagement,
deleteVehicleManagement,
getVehicleManagementById,
getVehicleByFilter
 }
from '../../controllers/election/VehicleManagementController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef CreateVehicleManagement
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createVehicleManagement
* @group VehicleManagement API Layer - Operations on VehicleManagement
* @param {CreateVehicleManagement.model} CreateVehicleManagement.body.required - Create VehicleManagement
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/createVehicleManagement', createVehicleManagement);


/**
* This function comment is parsed by TroonDx
* @route GET /getAllVehicleManagement
* @group VehicleManagement API Layer - Operations on VehicleManagement
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAllVehicleManagement',  getAllVehicleManagement);

/**
* @typedef UpdatebyIdVehicleManagement
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIdVehicleManagement
* @group VehicleManagement API Layer - Update Operations on VehicleManagement
* @param {UpdatebyIdVehicleManagement.model} UpdatebyIdVehicleManagement.body.required - UpdatebyID VehicleManagement
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIdVehicleManagement/:id', updateVehicleManagement);

/**
* @typedef DeleteByIdVehicleManagement
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdVehicleManagement
* @group VehicleManagement API Layer - Delete Operations on VehicleManagement
* @param {DeleteByIdVehicleManagement.model} DeleteByIdVehicleManagement.body.required - DeleteById VehicleManagement
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdVehicleManagement/:id', deleteVehicleManagement);

/**
* @typedef GetVehicleManagementById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getVehicleManagementById
* @group VehicleManagement API Layer - Get Operations on VehicleManagement
* @param {GetVehicleManagementById.model} GetVehicleManagementById.body.required - GetVehicleManagementById VehicleManagement
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getVehicleManagementById/:id',getVehicleManagementById);

router.get('/getVehicleByFilter/:district_id/:constituency_id',getVehicleByFilter);


export default router;
