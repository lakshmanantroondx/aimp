//app/routes/PartyRoute.js

import express from 'express';

const multer = require('multer');

const leaderimgstorage = multer.diskStorage({
   destination: (req, file, cb) => {
      cb(null, __basedir + '/uploads/leader/')
   },
   filename: (req, file, cb) => {
      cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname)
   }
});

const partyimgstorage = multer.diskStorage({
    destination: (req, file, cb) => {
       cb(null, __basedir + '/uploads/party/')
    },
    filename: (req, file, cb) => {
       cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname)
    }
 });
const uploadleaderimge = multer({ storage : leaderimgstorage });

const uploadpartyimagestrge = multer({ storage : partyimgstorage});

import {
createParty,
getAllParty,
updateParty,
deleteParty,
getPartyById,
uploadleaderimage,
uploadpartyimage }
from '../../controllers/election/PartyController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef CreateParty
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createParty
* @group Party API Layer - Operations on Party
* @param {CreateParty.model} CreateParty.body.required - Create Party
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/createParty', createParty);


/**
* This function comment is parsed by TroonDx
* @route GET /getAllParty
* @group Party API Layer - Operations on Party
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAllParty',  getAllParty);

/**
* @typedef UpdatebyIdParty
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIdParty
* @group Party API Layer - Update Operations on Party
* @param {UpdatebyIdParty.model} UpdatebyIdParty.body.required - UpdatebyID Party
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIdParty', updateParty);

/**
* @typedef DeleteByIdParty
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdParty
* @group Party API Layer - Delete Operations on Party
* @param {DeleteByIdParty.model} DeleteByIdParty.body.required - DeleteById Party
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdParty', deleteParty);

/**
* @typedef GetPartyById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getPartyById
* @group Party API Layer - Get Operations on Party
* @param {GetPartyById.model} GetPartyById.body.required - GetPartyById Party
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/getPartyById',getPartyById);

router.post('/uploadleaderimage/', uploadleaderimge.single('leaderimage'), uploadleaderimage);  

router.post('/uploadpartyimage/', uploadpartyimagestrge.single('partyimage'), uploadpartyimage);  

export default router;
