//app/routes/BoothHistoryRoute.js

import express from 'express';

import {
createBoothHistory,
getAllBoothHistory,
updateBoothHistory,
deleteBoothHistory,
getBoothHistoryById,
getboothhistorybyfilter }
from '../../controllers/election/BoothHistoryController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef CreateBoothHistory
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createBoothHistory
* @group BoothHistory API Layer - Operations on BoothHistory
* @param {CreateBoothHistory.model} CreateBoothHistory.body.required - Create BoothHistory
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/createBoothHistory', createBoothHistory);


/**
* This function comment is parsed by TroonDx
* @route GET /getAllBoothHistory
* @group BoothHistory API Layer - Operations on BoothHistory
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAllBoothHistory',  getAllBoothHistory);

/**
* @typedef UpdatebyIdBoothHistory
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIdBoothHistory
* @group BoothHistory API Layer - Update Operations on BoothHistory
* @param {UpdatebyIdBoothHistory.model} UpdatebyIdBoothHistory.body.required - UpdatebyID BoothHistory
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIdBoothHistory', updateBoothHistory);

/**
* @typedef DeleteByIdBoothHistory
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdBoothHistory
* @group BoothHistory API Layer - Delete Operations on BoothHistory
* @param {DeleteByIdBoothHistory.model} DeleteByIdBoothHistory.body.required - DeleteById BoothHistory
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdBoothHistory', deleteBoothHistory);

/**
* @typedef GetBoothHistoryById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getBoothHistoryById
* @group BoothHistory API Layer - Get Operations on BoothHistory
* @param {GetBoothHistoryById.model} GetBoothHistoryById.body.required - GetBoothHistoryById BoothHistory
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/getBoothHistoryById',getBoothHistoryById);

router.post('/getboothhistorybyfilter',getboothhistorybyfilter);

export default router;
