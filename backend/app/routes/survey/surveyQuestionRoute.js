//app/routes/surveyQuestionRoute.js

import express from 'express';

import {
createsurveyQuestion,
getAllsurveyQuestion,
updatesurveyQuestion,
deletesurveyQuestion,
getsurveyQuestionById,
getsurveyOptionById,
submitSurvey,
surveyDashboard,
getProfile,
listQuestion }
from '../../controllers/survey/surveyQuestionController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef CreatesurveyQuestion
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createsurveyQuestion
* @group surveyQuestion API Layer - Operations on surveyQuestion
* @param {CreatesurveyQuestion.model} CreatesurveyQuestion.body.required - Create surveyQuestion
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/createsurveyQuestion', createsurveyQuestion);


/**
* This function comment is parsed by TroonDx
* @route GET /getAllsurveyQuestion
* @group surveyQuestion API Layer - Operations on surveyQuestion
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAllsurveyQuestion',  getAllsurveyQuestion);

/**
* @typedef UpdatebyIdsurveyQuestion
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIdsurveyQuestion
* @group surveyQuestion API Layer - Update Operations on surveyQuestion
* @param {UpdatebyIdsurveyQuestion.model} UpdatebyIdsurveyQuestion.body.required - UpdatebyID surveyQuestion
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIdsurveyQuestion/:id', updatesurveyQuestion);

/**
* @typedef DeleteByIdsurveyQuestion
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdsurveyQuestion
* @group surveyQuestion API Layer - Delete Operations on surveyQuestion
* @param {DeleteByIdsurveyQuestion.model} DeleteByIdsurveyQuestion.body.required - DeleteById surveyQuestion
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdsurveyQuestion/:id', deletesurveyQuestion);

/**
* @typedef GetsurveyQuestionById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getsurveyQuestionById
* @group surveyQuestion API Layer - Get Operations on surveyQuestion
* @param {GetsurveyQuestionById.model} GetsurveyQuestionById.body.required - GetsurveyQuestionById surveyQuestion
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getsurveyQuestionById/:id',getsurveyQuestionById);

router.get('/getsurveyOptionById/:id',getsurveyOptionById);

router.get('/listQuestion/:id', listQuestion);

router.post('/submitSurvey/:id',submitSurvey);

router.post('/surveyDashboard',surveyDashboard);

router.post('/getProfile',getProfile);
export default router;
