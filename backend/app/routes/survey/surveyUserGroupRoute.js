//app/routes/surveyUserGroupRoute.js

import express from 'express';

import {
createsurveyUserGroup,
getAllsurveyUserGroup,
updatesurveyUserGroup,
deletesurveyUserGroup,
getsurveyUserGroupById,
getsurveyUserGroupByFilter }
from '../../controllers/survey/surveyUserGroupController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef CreatesurveyUserGroup
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createsurveyUserGroup
* @group surveyUserGroup API Layer - Operations on surveyUserGroup
* @param {CreatesurveyUserGroup.model} CreatesurveyUserGroup.body.required - Create surveyUserGroup
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/createsurveyUserGroup', createsurveyUserGroup);


/**
* This function comment is parsed by TroonDx
* @route GET /getAllsurveyUserGroup
* @group surveyUserGroup API Layer - Operations on surveyUserGroup
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAllsurveyUserGroup',  getAllsurveyUserGroup);

/**
* @typedef UpdatebyIdsurveyUserGroup
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIdsurveyUserGroup
* @group surveyUserGroup API Layer - Update Operations on surveyUserGroup
* @param {UpdatebyIdsurveyUserGroup.model} UpdatebyIdsurveyUserGroup.body.required - UpdatebyID surveyUserGroup
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIdsurveyUserGroup/:id', updatesurveyUserGroup);

/**
* @typedef DeleteByIdsurveyUserGroup
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdsurveyUserGroup
* @group surveyUserGroup API Layer - Delete Operations on surveyUserGroup
* @param {DeleteByIdsurveyUserGroup.model} DeleteByIdsurveyUserGroup.body.required - DeleteById surveyUserGroup
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdsurveyUserGroup/:id', deletesurveyUserGroup);

/**
* @typedef GetsurveyUserGroupById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getsurveyUserGroupById
* @group surveyUserGroup API Layer - Get Operations on surveyUserGroup
* @param {GetsurveyUserGroupById.model} GetsurveyUserGroupById.body.required - GetsurveyUserGroupById surveyUserGroup
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getsurveyUserGroupById/:id',getsurveyUserGroupById);

router.get('/getsurveyUserGroupByFilter/:district_id/:constituency_id',getsurveyUserGroupByFilter);


export default router;
