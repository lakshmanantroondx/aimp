//app/routes/surveyUserRoute.js

import express from 'express';

import {
createsurveyUser,
getAllsurveyUser,
updatesurveyUser,
deletesurveyUser,
getsurveyUserById }
from '../../controllers/survey/surveyUserController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef CreatesurveyUser
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createsurveyUser
* @group surveyUser API Layer - Operations on surveyUser
* @param {CreatesurveyUser.model} CreatesurveyUser.body.required - Create surveyUser
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/createsurveyUser', createsurveyUser);


/**
* This function comment is parsed by TroonDx
* @route GET /getAllsurveyUser
* @group surveyUser API Layer - Operations on surveyUser
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAllsurveyUser',  getAllsurveyUser);

/**
* @typedef UpdatebyIdsurveyUser
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIdsurveyUser
* @group surveyUser API Layer - Update Operations on surveyUser
* @param {UpdatebyIdsurveyUser.model} UpdatebyIdsurveyUser.body.required - UpdatebyID surveyUser
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIdsurveyUser/:id', updatesurveyUser);

/**
* @typedef DeleteByIdsurveyUser
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdsurveyUser
* @group surveyUser API Layer - Delete Operations on surveyUser
* @param {DeleteByIdsurveyUser.model} DeleteByIdsurveyUser.body.required - DeleteById surveyUser
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdsurveyUser/:id', deletesurveyUser);

/**
* @typedef GetsurveyUserById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getsurveyUserById
* @group surveyUser API Layer - Get Operations on surveyUser
* @param {GetsurveyUserById.model} GetsurveyUserById.body.required - GetsurveyUserById surveyUser
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getsurveyUserById/:id',getsurveyUserById);

export default router;
