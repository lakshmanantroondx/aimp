//app/routes/usermanagementRoute.js

import express from 'express';

import {
createusermanagement,
getAllusermanagement,
updateusermanagement,
deleteusermanagement,
getusermanagementById }
from '../../controllers/settings/usermanagementController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef Createusermanagement
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createusermanagement
* @group usermanagement API Layer - Operations on usermanagement
* @param {Createusermanagement.model} Createusermanagement.body.required - Create usermanagement
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/createusermanagement', createusermanagement);


/**
* This function comment is parsed by TroonDx
* @route GET /getAllusermanagement
* @group usermanagement API Layer - Operations on usermanagement
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
// router.get('/getAllusermanagement',verifyAuth,  getAllusermanagement);
router.get('/getAllusermanagement',  getAllusermanagement);

/**
* @typedef UpdatebyIdusermanagement
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIdusermanagement
* @group usermanagement API Layer - Update Operations on usermanagement
* @param {UpdatebyIdusermanagement.model} UpdatebyIdusermanagement.body.required - UpdatebyID usermanagement
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIdusermanagement/:id', updateusermanagement);

/**
* @typedef DeleteByIdusermanagement
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdusermanagement
* @group usermanagement API Layer - Delete Operations on usermanagement
* @param {DeleteByIdusermanagement.model} DeleteByIdusermanagement.body.required - DeleteById usermanagement
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdusermanagement/:id', deleteusermanagement);

/**
* @typedef GetusermanagementById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getusermanagementById
* @group usermanagement API Layer - Get Operations on usermanagement
* @param {GetusermanagementById.model} GetusermanagementById.body.required - GetusermanagementById usermanagement
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getusermanagementById/:id',getusermanagementById);

export default router;
