//app/routes/rolemanagementRoute.js

import express from 'express';

import {
createrolemanagement,
getAllrolemanagement,
updaterolemanagement,
deleterolemanagement,
getrolemanagementById }
from '../../controllers/settings/rolemanagementController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef Createrolemanagement
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createrolemanagement
* @group rolemanagement API Layer - Operations on rolemanagement
* @param {Createrolemanagement.model} Createrolemanagement.body.required - Create rolemanagement
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/createrolemanagement', createrolemanagement);


/**
* This function comment is parsed by TroonDx
* @route GET /getAllrolemanagement
* @group rolemanagement API Layer - Operations on rolemanagement
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAllrolemanagement',  getAllrolemanagement);

/**
* @typedef UpdatebyIdrolemanagement
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIdrolemanagement
* @group rolemanagement API Layer - Update Operations on rolemanagement
* @param {UpdatebyIdrolemanagement.model} UpdatebyIdrolemanagement.body.required - UpdatebyID rolemanagement
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIdrolemanagement/:id', updaterolemanagement);

/**
* @typedef DeleteByIdrolemanagement
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdrolemanagement
* @group rolemanagement API Layer - Delete Operations on rolemanagement
* @param {DeleteByIdrolemanagement.model} DeleteByIdrolemanagement.body.required - DeleteById rolemanagement
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdrolemanagement/:id', deleterolemanagement);

/**
* @typedef GetrolemanagementById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getrolemanagementById
* @group rolemanagement API Layer - Get Operations on rolemanagement
* @param {GetrolemanagementById.model} GetrolemanagementById.body.required - GetrolemanagementById rolemanagement
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getrolemanagementById/:id',getrolemanagementById);

export default router;
