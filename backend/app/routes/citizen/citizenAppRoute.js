//app/routes/citizenAppRoute.js

import express from 'express';

import {
    readallvotergrievance,
    readCitizenById,
    readvotergrievance,
    viewCandidate,
    readallsurveyquestionbyID,
    createvotergrievance,
    updateCitizenProfileById
}
    from '../../controllers/citizen/citizenAppController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()

router.get('/readallvotergrievance', readallvotergrievance);

router.get('/readCitizenById/:id', readCitizenById);

router.post('/readvotergrievance', readvotergrievance);

router.post('/viewCandidate', viewCandidate);

router.get('/readallsurveyquestionbyID/:districtid/:constituencyid', readallsurveyquestionbyID);

router.post('/createvotergrievance', createvotergrievance);

router.post('/updateCitizenProfileById/:id', updateCitizenProfileById);

export default router;
