//app/routes/userRoute.js

import express from 'express';

import {
createuser,
getAlluser,
updateuser,
deleteuser,
getuserById,
surveylogin,
userlogin,
createuserassociation,
getUserAssociationByUserId,
updateUserAssociation,
createCitizen ,
usercreate}
from '../../controllers/authentication/userController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef Createuser
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createuser
* @group user API Layer - Operations on user
* @param {Createuser.model} Createuser.body.required - Create user
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/createuser', verifyAuth, createuser);


/**
* This function comment is parsed by TroonDx
* @route GET /getAlluser
* @group user API Layer - Operations on user
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAlluser',verifyAuth,  getAlluser);

/**
* @typedef UpdatebyIduser
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIduser
* @group user API Layer - Update Operations on user
* @param {UpdatebyIduser.model} UpdatebyIduser.body.required - UpdatebyID user
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIduser', verifyAuth, updateuser);

/**
* @typedef DeleteByIduser
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIduser
* @group user API Layer - Delete Operations on user
* @param {DeleteByIduser.model} DeleteByIduser.body.required - DeleteById user
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIduser', verifyAuth, deleteuser);

/**
* @typedef GetuserById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getuserById
* @group user API Layer - Get Operations on user
* @param {GetuserById.model} GetuserById.body.required - GetuserById user
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getuserById/:id', verifyAuth,getuserById);

router.post('/surveylogin',surveylogin);

router.post('/userlogin',userlogin);

router.post('/createuserassociation',createuserassociation);

router.get('/getUserAssociationByUserId/:id',getUserAssociationByUserId);

router.post('/updateuserassociation/:id',updateUserAssociation);

router.post('/createCitizen',createCitizen);

router.post('/usercreate',usercreate);

export default router;
