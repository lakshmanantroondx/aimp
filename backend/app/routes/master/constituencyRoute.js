//app/routes/constituencyRoute.js

import express from 'express';


const multer = require('multer');

// -> Multer Upload Storage
const storage = multer.diskStorage({
   destination: (req, file, cb) => {
      cb(null, __basedir + '/uploads/constituency/')
   },
   filename: (req, file, cb) => {
      cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname)
   }
});
const bulkstorage = multer.diskStorage({
   destination: (req, file, cb) => {
      cb(null, __basedir + '/uploads/bulkupload/')
   },
   filename: (req, file, cb) => {
      cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname)
   }
});

const uploadimge = multer({ storage : storage });
const constituencyBulk = multer({ storage : bulkstorage });

import {
createconstituency,
getAllconstituency,
updateconstituency,
deleteconstituency,
getconstituencyById,
uploadimage,
bulkupload,
getconstituencyByDistrictId,
downloadboothHisTemplate,
downloadboothTemplate,
uploadBulkbooth,
downloadVoterTemplate,
uploadBulkvoter,
uploadBulkboothhis
}
from '../../controllers/master/constituencyController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef Createconstituency
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createconstituency
* @group constituency API Layer - Operations on constituency
* @param {Createconstituency.model} Createconstituency.body.required - Create constituency
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/createconstituency', createconstituency);


/**
* This function comment is parsed by TroonDx
* @route GET /getAllconstituency
* @group constituency API Layer - Operations on constituency
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAllconstituency',  getAllconstituency);

/**
* @typedef UpdatebyIdconstituency
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIdconstituency
* @group constituency API Layer - Update Operations on constituency
* @param {UpdatebyIdconstituency.model} UpdatebyIdconstituency.body.required - UpdatebyID constituency
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIdconstituency/:id', updateconstituency);

/**
* @typedef DeleteByIdconstituency
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdconstituency
* @group constituency API Layer - Delete Operations on constituency
* @param {DeleteByIdconstituency.model} DeleteByIdconstituency.body.required - DeleteById constituency
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdconstituency/:id', deleteconstituency);

/**
* @typedef GetconstituencyById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getconstituencyById
* @group constituency API Layer - Get Operations on constituency
* @param {GetconstituencyById.model} GetconstituencyById.body.required - GetconstituencyById constituency
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getconstituencyById/:id',getconstituencyById);

router.get('/getconstituencyByDistrictId/:id',getconstituencyByDistrictId);

router.post('/uploadconstituencyimage/', uploadimge.single('constituencyimage'), uploadimage);   

router.post('/uploadBulkconstituency', constituencyBulk.single("constituencybulk"), bulkupload); 

router.get('/downloadboothHisTemplate/:party_id/:election_period_id',downloadboothHisTemplate);

router.get('/downloadboothTemplate/:constituency_id/:ward_id',downloadboothTemplate);

router.post('/uploadBulkbooth', constituencyBulk.single("boothbulk"), uploadBulkbooth);

router.get('/downloadVoterTemplate/',downloadVoterTemplate);

router.post('/uploadBulkvoter', constituencyBulk.single("voterbulk"), uploadBulkvoter);

router.post('/uploadBulkboothhis', constituencyBulk.single("boothbulkhis"), uploadBulkboothhis);

export default router;