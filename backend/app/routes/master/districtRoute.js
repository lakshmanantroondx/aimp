//app/routes/districtRoute.js

import express from 'express';

import {
createdistrict,
getAlldistrict,
updatedistrict,
deletedistrict,
getdistrictById }
from '../../controllers/master/districtController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef Createdistrict
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createdistrict
* @group district API Layer - Operations on district
* @param {Createdistrict.model} Createdistrict.body.required - Create district
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/createdistrict', createdistrict);


/**
* This function comment is parsed by TroonDx
* @route GET /getAlldistrict
* @group district API Layer - Operations on district
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAlldistrict',  getAlldistrict);

/**
* @typedef UpdatebyIddistrict
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIddistrict
* @group district API Layer - Update Operations on district
* @param {UpdatebyIddistrict.model} UpdatebyIddistrict.body.required - UpdatebyID district
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIddistrict/:id', updatedistrict);

/**
* @typedef DeleteByIddistrict
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIddistrict
* @group district API Layer - Delete Operations on district
* @param {DeleteByIddistrict.model} DeleteByIddistrict.body.required - DeleteById district
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIddistrict/:id', deletedistrict);

/**
* @typedef GetdistrictById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getdistrictById
* @group district API Layer - Get Operations on district
* @param {GetdistrictById.model} GetdistrictById.body.required - GetdistrictById district
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getdistrictById/:id',getdistrictById);

export default router;