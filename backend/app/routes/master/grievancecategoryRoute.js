//app/routes/grievancecategoryRoute.js

import express from 'express';

import {
creategrievancecategory,
getAllgrievancecategory,
updategrievancecategory,
deletegrievancecategory,
getgrievancecategoryById }
from '../../controllers/master/grievancecategoryController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef Creategrievancecategory
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /creategrievancecategory
* @group grievancecategory API Layer - Operations on grievancecategory
* @param {Creategrievancecategory.model} Creategrievancecategory.body.required - Create grievancecategory
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/creategrievancecategory', creategrievancecategory);


/**
* This function comment is parsed by TroonDx
* @route GET /getAllgrievancecategory
* @group grievancecategory API Layer - Operations on grievancecategory
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAllgrievancecategory',  getAllgrievancecategory);

/**
* @typedef UpdatebyIdgrievancecategory
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIdgrievancecategory
* @group grievancecategory API Layer - Update Operations on grievancecategory
* @param {UpdatebyIdgrievancecategory.model} UpdatebyIdgrievancecategory.body.required - UpdatebyID grievancecategory
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIdgrievancecategory/:id', updategrievancecategory);

/**
* @typedef DeleteByIdgrievancecategory
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdgrievancecategory
* @group grievancecategory API Layer - Delete Operations on grievancecategory
* @param {DeleteByIdgrievancecategory.model} DeleteByIdgrievancecategory.body.required - DeleteById grievancecategory
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdgrievancecategory/:id', deletegrievancecategory);

/**
* @typedef GetgrievancecategoryById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getgrievancecategoryById
* @group grievancecategory API Layer - Get Operations on grievancecategory
* @param {GetgrievancecategoryById.model} GetgrievancecategoryById.body.required - GetgrievancecategoryById grievancecategory
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getgrievancecategoryById/:id',getgrievancecategoryById);

export default router;