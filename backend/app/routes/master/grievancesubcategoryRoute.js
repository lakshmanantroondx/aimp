//app/routes/grievancesubcategoryRoute.js

import express from 'express';

import {
creategrievancesubcategory,
getAllgrievancesubcategory,
updategrievancesubcategory,
deletegrievancesubcategory,
getgrievancesubcategoryById }
from '../../controllers/master/grievancesubcategoryController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef Creategrievancesubcategory
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /creategrievancesubcategory
* @group grievancesubcategory API Layer - Operations on grievancesubcategory
* @param {Creategrievancesubcategory.model} Creategrievancesubcategory.body.required - Create grievancesubcategory
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/creategrievancesubcategory', creategrievancesubcategory);


/**
* This function comment is parsed by TroonDx
* @route GET /getAllgrievancesubcategory
* @group grievancesubcategory API Layer - Operations on grievancesubcategory
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAllgrievancesubcategory',  getAllgrievancesubcategory);

/**
* @typedef UpdatebyIdgrievancesubcategory
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIdgrievancesubcategory
* @group grievancesubcategory API Layer - Update Operations on grievancesubcategory
* @param {UpdatebyIdgrievancesubcategory.model} UpdatebyIdgrievancesubcategory.body.required - UpdatebyID grievancesubcategory
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIdgrievancesubcategory/:id', updategrievancesubcategory);

/**
* @typedef DeleteByIdgrievancesubcategory
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdgrievancesubcategory
* @group grievancesubcategory API Layer - Delete Operations on grievancesubcategory
* @param {DeleteByIdgrievancesubcategory.model} DeleteByIdgrievancesubcategory.body.required - DeleteById grievancesubcategory
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdgrievancesubcategory/:id', deletegrievancesubcategory);

/**
* @typedef GetgrievancesubcategoryById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getgrievancesubcategoryById
* @group grievancesubcategory API Layer - Get Operations on grievancesubcategory
* @param {GetgrievancesubcategoryById.model} GetgrievancesubcategoryById.body.required - GetgrievancesubcategoryById grievancesubcategory
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getgrievancesubcategoryById/:id',getgrievancesubcategoryById);

export default router;