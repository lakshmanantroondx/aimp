//app/routes/religionRoute.js

import express from 'express';

import {
createreligion,
getAllreligion,
updatereligion,
deletereligion,
getreligionById }
from '../../controllers/master/religionController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef Createreligion
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createreligion
* @group religion API Layer - Operations on religion
* @param {Createreligion.model} Createreligion.body.required - Create religion
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/createreligion', createreligion);


/**
* This function comment is parsed by TroonDx
* @route GET /getAllreligion
* @group religion API Layer - Operations on religion
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAllreligion',  getAllreligion);

/**
* @typedef UpdatebyIdreligion
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIdreligion
* @group religion API Layer - Update Operations on religion
* @param {UpdatebyIdreligion.model} UpdatebyIdreligion.body.required - UpdatebyID religion
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIdreligion/:id', updatereligion);

/**
* @typedef DeleteByIdreligion
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdreligion
* @group religion API Layer - Delete Operations on religion
* @param {DeleteByIdreligion.model} DeleteByIdreligion.body.required - DeleteById religion
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdreligion/:id', deletereligion);

/**
* @typedef GetreligionById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getreligionById
* @group religion API Layer - Get Operations on religion
* @param {GetreligionById.model} GetreligionById.body.required - GetreligionById religion
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getreligionById/:id',getreligionById);

export default router;
