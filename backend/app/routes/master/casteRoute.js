//app/routes/casteRoute.js

import express from 'express';

import {
createcaste,
getAllcaste,
updatecaste,
deletecaste,
getcasteById }
from '../../controllers/master/casteController';

import verifyAuth from '../../middleware/verifyAuth';

const router = express.Router()


/**
* @typedef Createcaste
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createcaste
* @group caste API Layer - Operations on caste
* @param {Createcaste.model} Createcaste.body.required - Create caste
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/createcaste', createcaste);


/**
* This function comment is parsed by TroonDx
* @route GET /getAllcaste
* @group caste API Layer - Operations on caste
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAllcaste',  getAllcaste);

/**
* @typedef UpdatebyIdcaste
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIdcaste
* @group caste API Layer - Update Operations on caste
* @param {UpdatebyIdcaste.model} UpdatebyIdcaste.body.required - UpdatebyID caste
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIdcaste/:id', updatecaste);

/**
* @typedef DeleteByIdcaste
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdcaste
* @group caste API Layer - Delete Operations on caste
* @param {DeleteByIdcaste.model} DeleteByIdcaste.body.required - DeleteById caste
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdcaste/:id', deletecaste);

/**
* @typedef GetcasteById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getcasteById
* @group caste API Layer - Get Operations on caste
* @param {GetcasteById.model} GetcasteById.body.required - GetcasteById caste
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getcasteById/:id',getcasteById);

export default router;
