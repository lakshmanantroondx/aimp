//app/routes/wardRoute.js

import express from 'express';

import {
createward,
getAllward,
updateward,
deleteward,
getwardById,
wardbulkupload,
getwardByFilterId }
from '../../controllers/master/wardController';

import verifyAuth from '../../middleware/verifyAuth';
const multer = require('multer');
const router = express.Router()

const bulkstorage = multer.diskStorage({
    destination: (req, file, cb) => {
       cb(null, __basedir + '/uploads/bulkupload/')
    },
    filename: (req, file, cb) => {
       cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname)
    }
 });
 const wardBulk = multer({ storage : bulkstorage });

/**
* @typedef Createward
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
* @security JWT
*/
/**
* This function comment is parsed by TroonDx
* @route POST /createward
* @group ward API Layer - Operations on ward
* @param {Createward.model} Createward.body.required - Create ward
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

router.post('/createward', createward);


/**
* This function comment is parsed by TroonDx
* @route GET /getAllward
* @group ward API Layer - Operations on ward
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getAllward',  getAllward);

/**
* @typedef UpdatebyIdward
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /updateByIdward
* @group ward API Layer - Update Operations on ward
* @param {UpdatebyIdward.model} UpdatebyIdward.body.required - UpdatebyID ward
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/updateByIdward/:id', updateward);

/**
* @typedef DeleteByIdward
* @property {string} id.required - id - eg: 1
*/
/**
* This function comment is parsed by TroonDx
* @route POST /deleteByIdward
* @group ward API Layer - Delete Operations on ward
* @param {DeleteByIdward.model} DeleteByIdward.body.required - DeleteById ward
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.post('/deleteByIdward/:id', deleteward);

/**
* @typedef GetwardById
* @property {string} var1.required - username - eg: test1
* @property {string} var2.required - Org Name- eg: test2
* @property {string} var3.required - password eg: test3
* @property {string} var4.required -Role- eg: test4
* @property {string} var5.required - usermail - eg:admin@testing.com
*/
/**
* This function comment is parsed by TroonDx
* @route POST /getwardById
* @group ward API Layer - Get Operations on ward
* @param {GetwardById.model} GetwardById.body.required - GetwardById ward
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/
router.get('/getwardById/:id',getwardById);

router.get('/getwardByFilterId/:districtId/:constituencyId',getwardByFilterId);


router.post('/uploadBulkWard', wardBulk.single("wardbulk"), wardbulkupload);

export default router;
