import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';
import connection from '../../db/dev/pool'
import { isEmpty, empty } from '../../helpers/validations';

import { errorMessage, successMessage, status, } from '../../helpers/status';

/**
   * Create surveyQuestion
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const createsurveyQuestion = async (req, res) => {
    const { created_by, option_type, question, regional_question, TYPE, survey_user_group_id, options } = req.body;
    var created_date = moment(new Date());
    var delete_status = 0;
    var active_status = 1;
    var survey_question_id;
    const getQuery = "INSERT INTO  survey_question(created_by,option_type,question,regional_question,TYPE,survey_user_group_id,created_date,delete_status,active_status) VALUES (?,?,?,?,?,?,current_date(),0,1);";
    const optionQuery = 'INSERT INTO  survey_option(option_value,regional_option_value,survey_question_id,delete_status) VALUES(?,?,?,false);';
    connection.query(getQuery, [created_by, option_type, question, regional_question, TYPE, survey_user_group_id, created_date, delete_status, active_status], function (error, results, fields) {
        if (error) {
            console.log("response error", error);
            return res.status(status.error).send(error);
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "status": true, "error": errorMessage });
        } else {
            const dbResponse = results[0];
            survey_question_id = results.insertId;
            var i;
            for (i in options) {
                var option_value = options[i].option_value;
                var regional_option_value = options[i].regional_option_value;
                connection.query(optionQuery, [option_value, regional_option_value, survey_question_id], function (error, result) {
                    console.log("error resp -->", error);
                    console.log("result -->", result);
                });

            }
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }

    });

};

/**
 * Update surveyQuestion
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated surveyQuestion
 */

const updatesurveyQuestion = async (req, res) => {
    const { id } = req.params;
    const { option_type, question, regional_question, upadted_by, options } = req.body;

    const optiontype = req.body.option_type;
    const optionvalues = [options];
    const UpdateQuery = 'call updatesurveyQuestion(?,?,?,?,?);';
    const optionQuery = 'INSERT INTO  survey_option(option_value,regional_option_value,delete_status,survey_question_id) VALUES(?,?,false,?);';
    const deleteOptionQuery = 'call deletesurveyOption_ID(?);'
    if (optiontype == "CHECK_BOX" || optiontype == "RADIO_BUTTON") {
        //console.log("to update for req-->",id);
        const values = [
            id, option_type, question, regional_question, upadted_by
        ];
        connection.query(UpdateQuery, values, function (error, results, fields) {
            if (error) {
                console.log("response error", error);
                errorMessage.error = 'No data found';
                return res.status(status.error).send(errorMessage);
            }
            const dbResponse = results[0];
            connection.query(deleteOptionQuery, [id], function (error, result) {
                //console.log("error resp -->",error);
                //console.log("result -->",result);
            });

            var i;
            for (i in options) {
                var option_value = options[i].option_value;
                var regional_option_value = options[i].regional_option_value;
                connection.query(optionQuery, [option_value, regional_option_value, id], function (error, result) {
                    //console.log("error resp -->",error);
                    console.log("result -->", result);
                });
            }
            return res.status(status.success).send({ "status": true, "result": dbResponse });
        });

    } else {
        const values = [
            id, option_type, question, regional_question, upadted_by
        ];
        connection.query(UpdateQuery, values, function (error, results, fields) {
            if (error) {
                console.log("response error", error);
                errorMessage.error = 'No data found';
                return res.status(status.error).send(errorMessage);
            }
            const dbResponse = results[0];
            return res.status(status.success).send({ "status": true, "result": dbResponse });

        });

    }



};

/**
   * Delete surveyQuestion
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deletesurveyQuestion = async (req, res) => {
    const { id } = req.params;
    connection.query('call deletesurveyQuestion(?);', [id], function (error, results, fields) {
        if (error) {
            console.log("response error", error);
            errorMessage.error = 'No data found';
            return res.status(status.error).send(errorMessage);
        }
        const dbResponse = results[0];
        return res.status(status.success).send(dbResponse);

    });

};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAllsurveyQuestion = async (req, res) => {

    const getquery = 'call readallsurveyQuestion();'
    connection.query(getquery, [], function (error, results, fields) {
        if (error) {
            console.log("response error", error);
            return res.status(status.error).send(error);
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "status": true, "error": errorMessage });
        } else {
            const dbResponse = results;
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }
    });

};

/**
   * Get getsurveyQuestionById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getsurveyQuestionById = async (req, res) => {
    const { id } = req.params;
    const values = [id];
    const getquery = 'call readsurveyQuestion(?)';
    const getOptionQuery = 'call getOption(?);'
    connection.query(getquery, values, function (error, results, fields) {
        if (error) {
            console.log("response error", error);
            errorMessage.error = 'Exception in getsurveyQuestionById';
            return res.status(status.error).send({ "error": errorMessage, "status": false });
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send(errorMessage);
        }
        else {
            const dbResponse = {};
            dbResponse.data = results[0];
            const option_type = results[0][0].option_type;
            const questioid = results[0][0].id;
            var options = [];
            var data = [];
            if (results[0][0].option_type == "CHECK_BOX" || results[0][0].option_type == "RADIO_BUTTON") {
                //console.log("id -->",results[0][0].id);
                //console.log("option_type ->",results[0][0].option_type);
                connection.query(getOptionQuery, questioid, function (error, results) {
                    if (error) {
                        console.log("response error", error);
                        errorMessage.error = 'Exception in getsurveyQuestionById';
                        return res.status(status.error).send({ "error": errorMessage, "status": false });
                    } else {
                        console.log("result from options ", results[0]);
                        options = results[0];
                        dbResponse.options = options;
                        return res.status(status.success).send({ "results": dbResponse, "status": true });
                    }
                })

            } else {
                dbResponse.data = results[0];
                return res.status(status.success).send({ "results": dbResponse, "status": true });
            }
        }

    });
};

const getsurveyOptionById = async (req, res) => {
    const { id } = req.params;
    const values = [id];
    const getquery = 'call readsurveyOption_on_questionID(?);';
    connection.query(getquery, values, function (error, results, fields) {
        if (error) {
            console.log("response error", error);
            errorMessage.error = 'Exception in getsurveyOptionById';
            return res.status(status.error).send({ "error": errorMessage, "status": false });
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send(errorMessage);
        }
        else {
            const dbResponse = results[0];
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }

    });
};

const listQuestion = async (req, res) => {
    const { id } = req.params;
    const values = [id];
    // const getquery = 'call readsurveyQuestionbyid(?);';
    const getquery = `select sq.id,sq.question,sq.option_type as "optionType",sq.regional_question as "regionalQuestion", 
    sq.survey_user_group_id as "surveyUserGroupID" 
    from survey_question sq
    join survey_user su on su.survey_user_group_id=sq.survey_user_group_id where su.id = ?`
    connection.query(getquery, values, function (error, results, fields) {
        if (error) {
            console.log("response error", error);
            errorMessage.error = 'Exception in readsurveyQuestionbyid';
            return res.status(status.error).send({ "error": errorMessage, "status": false });
        }
        else {
            var selectoption = `select so.id as "OptionId", so.option_value as "OptionValue", so.regional_option_value as "regionalOptionValue" from survey_option so where so.survey_question_id=?`
            let finalresults = [];
            var loop = 0;
            var totallen=results.length;
            results.forEach(async (row, index) => {
                console.log("Row ",row)
                connection.query(selectoption, row.id, function (error, resu, fields) {
                    row.options = resu;
                    if(error){
                        console.log("Error",error)
                    }
                    finalresults.push(row);
                    loop = loop + 1
                    console.log(loop,index)
                    if (loop == totallen) {
                        const dbResponse = finalresults;
                        return res.status(status.success).send({ "results": dbResponse, "status": true });
                    }
                });
            });
            // const dbResponse=results;
            // return res.status(status.success).send({"results": dbResponse, "status": true});
        }

    });
};

const submitSurvey = async (req, res) => {
    const { id } = req.params;
    const { userId, user, survey } = req.body;
    var createdBy = user.created_by;
    var gender = user.gender;
    var work_type = user.work_type;
    var age = user.age;
    var survey_address = user.survey_address;
    var mobile_no = user.mobile_no;
    var latitude = user.latitude;
    var longitude = user.longitude;
    var pin_code = user.pin_code;
    //console.log("error ->",createdBy,gender,work_type,age,survey_address,mobile_no);
    var i;
    const Examinevalues = [createdBy, gender, work_type, age, survey_address, mobile_no, latitude, longitude, pin_code];
    const Examinequery = 'call createsurveyexamine(?,?,?,?,?,?,?,?,?)';
    const detailQuery = 'call createsurveyDetails(?,?,?,?,?);'
    connection.query(Examinequery, Examinevalues, function (error, results, fields) {
        if (error) {
            console.log("response error", error);
            errorMessage.error = 'Exception in getsurveyOptionById';
            return res.status(status.error).send({ "error": errorMessage, "status": false });
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send(errorMessage);
        }
        else {
            const dbResponse = results[0][0].insert_id;
            //console.log("examine ID--->",dbResponse);
            const Examine_ID = dbResponse;
            for (i in survey) {
                var question_id = survey[i].questionId;
                var survey_by = survey[i].survey_by;
                var option_id = survey[i].optionId;
                var _answer = survey[i].answer;
                var detailvalues = [_answer, survey_by, Examine_ID, option_id, question_id];
                connection.query(detailQuery, detailvalues, function (error, results) {
                    if (error) {
                        errorMessage.error = 'Exception in submit survey';
                        //return res.status(status.error).send({ "error": errorMessage, "status": false });
                    } else {
                        const response = results[0];
                    }
                });
            }
            return res.status(status.success).send({ "successMsg": "InsertedSuccessfully", "status": true });
        }
    });
};


const surveyDashboard = async (req, res) => {
    const { userid } = req.body;
    const values = [userid];
    const dashboardquery = 'call surveyDashboard(?);';
    connection.query(dashboardquery, values, function (error, results, fields) {
        if (error) {
            console.log("response error", error);
            errorMessage.error = 'Exception in surveyDashboard';
            return res.status(status.error).send({ "error": errorMessage, "status": false });
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send(errorMessage);
        }
        else {
            const dbResponse = results[0][0];
            return res.status(status.success).send({ "data": dbResponse, "status": true });
        }
    });
};

const getProfile = async (req, res) => {
    const { userid } = req.body;
    const values = [userid];
    const dashboardquery = 'call getProfile(?);';
    connection.query(dashboardquery, values, function (error, results, fields) {
        if (error) {
            console.log("response error", error);
            errorMessage.error = 'Exception in getProfile';
            return res.status(status.error).send({ "error": errorMessage, "status": false });
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send(errorMessage);
        }
        else {
            const response = results[0][0];
            return res.status(status.success).send({ "data": response, "status": true });
        }
    });
};

export {
    createsurveyQuestion,
    getAllsurveyQuestion,
    updatesurveyQuestion,
    deletesurveyQuestion,
    getsurveyQuestionById,
    getsurveyOptionById,
    submitSurvey,
    surveyDashboard,
    getProfile,
    listQuestion


}
