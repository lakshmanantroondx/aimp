import moment from 'moment';
import connection from '../../db/dev/pool'
import {isEmpty, empty,hashPassword } from '../../helpers/validations';

import {errorMessage,successMessage,status,} from '../../helpers/status';

/**
   * Create surveyUser
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const createsurveyUser = async (req, res) => {
    const { email,mobile,name,password,regional_name,user_name,created_by,survey_user_group_id } = req.body;
    var hasedPassword = await hashPassword(password);
    //console.log("has=hed ->",hasedPassword);
    const values = [
        email,mobile,name,hasedPassword,regional_name,user_name,created_by,survey_user_group_id
    ];

    const getquery = 'call createsurveyUser(?,?,?,?,?,?,?,?)';
    connection.query(getquery,values, function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });



};

/**
 * Update surveyUser
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated surveyUser
 */

const updatesurveyUser = async (req, res) => {
 const { id } = req.params;  
 const { age,email,mobile,name,regional_name,updated_by } = req.body;

 const UpdateQuery = 'call updatesurveyUser(?,?,?,?,?,?,?)';

 const updatevalues = [
     id,age,email,mobile,name,regional_name,updated_by
 ];
    connection.query(UpdateQuery,updatevalues, function (error, results, fields) {
        if (error){
            console.log("response error",error);
        errorMessage.error = 'No data found';
        return res.status(status.error).send(errorMessage);
        } 
        const dbResponse=results[0];
        return res.status(status.success).send({"status" : true, "result" : dbResponse});

      });
};

/**
   * Delete surveyUser
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deletesurveyUser = async (req, res) => {
    const { id } = req.params;
        connection.query('call deletesurveyUser(?);',[id], function (error, results, fields) {
            if (error){
                console.log("response error",error);
            errorMessage.error = 'No data found';
            return res.status(status.error).send(errorMessage);
            } 
            const dbResponse=results;
            return res.status(status.success).send(results);

          });

};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAllsurveyUser = async (req, res) => {

    const getquery = 'call readsurveyUserall();'
    connection.query(getquery,[], function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results;
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });

};

/**
   * Get getsurveyUserById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getsurveyUserById = async (req, res) => {
    const { id } = req.params;
    const values = [id];
    const getquery = 'call readsurveyUser(?)';
        connection.query(getquery,values, function (error, results, fields) {
            if (error){
                console.log("response error",error);
            errorMessage.error = 'Exception in getsurveyUserById';
            return res.status(status.error).send({"error" : errorMessage, "status" : false});
            } 
            else if(results[0] == 0 ){
                errorMessage.error = 'No data found';
                return res.status(status.error).send(errorMessage);
            }
            else {
                const dbResponse=results[0];
                return res.status(status.success).send({"results": dbResponse, "status": true});
            }
            
          });
};

export {
    createsurveyUser,
    getAllsurveyUser,
    updatesurveyUser,
    deletesurveyUser,
    getsurveyUserById
}
