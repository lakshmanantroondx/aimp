import moment from 'moment';
import connection from '../../db/dev/pool'

import {isEmpty, empty } from '../../helpers/validations';

import {errorMessage,successMessage,status,} from '../../helpers/status';

/**
   * Create surveyUserGroup
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const createsurveyUserGroup = async (req, res) => {
    const { created_by,group_name,regional_group_name,target_count,target_date,district_id_in,constituency_id_in } = req.body;
    const values = [
        created_by,group_name,regional_group_name,target_count,target_date,district_id_in,constituency_id_in
    ];

    const createquery = 'call createsurveyUserGroup(?,?,?,?,?,?,?)';
    connection.query(createquery,values, function (error, results, fields) {
        if (error){
            console.log("error createsurveyUserGroup",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0][0];
            return res.status(status.success).send({"data": dbResponse, "status": true});
        }
      });
};

/**
 * Update surveyUserGroup
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated surveyUserGroup
 */

const updatesurveyUserGroup = async (req, res) => {
   const { id } = req.params;
   const { group_name,regional_group_name,updated_by,target_count,target_date,district_id,constituency_id } = req.body;
   const updatevalues = [
    id,group_name,regional_group_name,updated_by,target_count,target_date,district_id,constituency_id
   ]

   const usergroupQuery = 'call updatesurveyUserGroup(?,?,?,?,?,?,?,?);';

   connection.query(usergroupQuery,updatevalues, function (error, results, fields) {
    if (error){
        console.log("error in updatesurveyUserGroup",error);
    errorMessage.error = 'No data found';
    return res.status(status.error).send(errorMessage);
    } 
    const dbResponse=results[0][0];
    return res.status(status.success).send({"status" : true, "data" : dbResponse});

  });

};

/**
   * Delete surveyUserGroup
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deletesurveyUserGroup = async (req, res) => {
    const { id } = req.params;
        connection.query('call deletesurveyUserGroup(?);',[id], function (error, results, fields) {
            if (error){
                console.log("response error",error);
            errorMessage.error = 'No data found';
            return res.status(status.error).send(errorMessage);
            } 
            const dbResponse=results;
            return res.status(status.success).send({"status" : true, "data" : dbResponse});

          });

};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAllsurveyUserGroup = async (req, res) => {
    const getquery = 'call readallsurveyUserGroup();'
    connection.query(getquery,[], function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results;
            return res.status(status.success).send({"data": dbResponse, "status": true});
        }
      });
};

/**
   * Get getsurveyUserGroupById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getsurveyUserGroupById = async (req, res) => {
    const { id } = req.params;
    const values = [id];
    const getquery = 'call readsurveyUserGroup(?)';
        connection.query(getquery,values, function (error, results, fields) {
            if (error){
                console.log("response error",error);
            errorMessage.error = 'Exception in getsurveyUserGroupById';
            return res.status(status.error).send({"error" : errorMessage, "status" : false});
            } 
            else if(results[0] == 0 ){
                errorMessage.error = 'No data found';
                return res.status(status.error).send(errorMessage);
            } else {
                const dbResponse=results[0][0];
                return res.status(status.success).send({"data": dbResponse, "status": true});
            }
          });
};

/**
   * Get getsurveyUserGroupByFilter
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
  const getsurveyUserGroupByFilter = async (req, res) => {
    const { district_id,constituency_id } = req.params;
    const values = [district_id,constituency_id];
    const getquery = 'call readallSurveyUserGroupByFilter(?,?)';
        connection.query(getquery,values, function (error, results, fields) {
            if (error){
                console.log("response error",error);
            errorMessage.error = 'Exception in getsurveyUserGroupByFilter';
            return res.status(status.error).send({"error" : errorMessage, "status" : false});
            } 
            else if(results[0] == 0 ){
                errorMessage.error = 'No data found';
                return res.status(status.error).send(errorMessage);
            } else {
                const dbResponse=results[0];
                return res.status(status.success).send({"data": dbResponse, "status": true});
            }
          });
};


export {
    createsurveyUserGroup,
    getAllsurveyUserGroup,
    updatesurveyUserGroup,
    deletesurveyUserGroup,
    getsurveyUserGroupById,
    getsurveyUserGroupByFilter
}
