import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';
import connection from '../../db/dev/pool'
import { isEmpty, empty } from '../../helpers/validations';

import { errorMessage, successMessage, status, } from '../../helpers/status';





const readallvotergrievance = async (req, res) => {

    const getquery = 'call readallvotergrievance();'
    connection.query(getquery, [], function (error, results, fields) {
        if (error) {
            console.log("response error", error);
            return res.status(status.error).send(error);
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "status": true, "error": errorMessage });
        } else {
            const dbResponse = results[0];
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }
    });

};


const readCitizenById = async (req, res) => {

    const { id } = req.params;
    const getquery = 'call  readgeneralvoter(?);'
    const votervalue = [id]
    connection.query(getquery, votervalue, function (error, results, fields) {
        if (error) {
            console.log("response error", error);
            return res.status(status.error).send(error);
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "status": true, "error": errorMessage });
        } else {
            const dbResponse = results[0];
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }
    });

};



const readvotergrievance = async (req, res) => {

    const { id } = req.body;
    const getquery = 'call readvotergrievance(?);'
    const votervalue = [id]
    connection.query(getquery, votervalue, function (error, results, fields) {
        if (error) {
            console.log("response error", error);
            return res.status(status.error).send(error);
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "status": true, "error": errorMessage });
        } else {
            const dbResponse = results[0];
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }
    });

};


const viewCandidate = async (req, res) => {

    const { district, constituency } = req.body;
    const candidatequery = 'call viewCandidateList(?,?);'
    const candidatevalue = [district, constituency]
    connection.query(candidatequery, candidatevalue, function (error, results, fields) {
        if (error) {
            console.log("response error", error);
            return res.status(status.error).send(error);
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "status": true, "error": errorMessage });
        } else {
            const dbResponse = results[0];
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }
    });

};

//call readallsurveyquestionbyfilter(5,6)

const readallsurveyquestionbyID = async (req, res) => {
    const { districtid, constituencyid } = req.params;
    const values = [districtid, constituencyid];
    // const getquery = 'call readsurveyQuestionbyid(?);';
    const getquery = `select distinct sq.id,sq.question,sq.option_type as "optionType",sq.regional_question as "regionalQuestion", 
    sq.survey_user_group_id as "surveyUserGroupID" 
    from survey_question sq
    join survey_user su on su.survey_user_group_id=sq.survey_user_group_id 
    join survey_user_group sug on sug.id=su.survey_user_group_id
    join constituency c on sug.constituency_id=c.id
    join district d on sug.district_id=d.id 
    where sug.district_id = ? and sug.constituency_id= ?`
    connection.query(getquery, values, function (error, results, fields) {
        if (error) {
            console.log("response error", error);
            errorMessage.error = 'Exception in readsurveyQuestionbyid';
            return res.status(status.error).send({ "error": errorMessage, "status": false });
        }
        else {
            var selectoption = `select so.id as "OptionId", so.option_value as "OptionValue", so.regional_option_value as "regionalOptionValue" from survey_option so where so.survey_question_id=?`
            let finalresults = [];
            var loop = 0;
            var totallen = results.length;
            results.forEach(async (row, index) => {
                connection.query(selectoption, row.id, function (error, resu, fields) {
                    row.options = resu;
                    if (error) {
                        console.log("Error", error)
                    }
                    finalresults.push(row);
                    loop = loop + 1
                    if (loop == totallen) {
                        const dbResponse = finalresults;
                        return res.status(status.success).send({ "results": dbResponse, "status": true });
                    }
                });
            });
        
        }

    });
}

// const readallsurveyquestionbyID = async (req,res) => {

//     const { constituencyid,districtid } = req.body;
//     const readquery = 'call readallsurveyquestionbyfilter(?,?)'
//     const readvalue = [ constituencyid,districtid ]
//     connection.query(readquery,readvalue, function (error, results, fields) {
//         if (error){
//             console.log("response error",error);
//         return res.status(status.error).send(error);
//         } 
//         else if(results[0] == 0 ){
//             errorMessage.error = 'No data found';
//             return res.status(status.error).send({"status": true, "error": errorMessage});
//         } else {
//             const dbResponse=results[0];
//             return res.status(status.success).send({"results": dbResponse, "status": true});
//         }
//       });

// }


const createvotergrievance = async (req, res) => {

    const { posterid, description, subcategoryid, voterid, latitude, longitude, image_url, district_id, constituency_id } = req.body;
    const createquery = 'call createvotergrievance(?,?,?,?,?,?,?,?,?);'
    const createvalue = [posterid, description, subcategoryid, voterid, latitude, longitude, image_url, district_id, constituency_id]
    connection.query(createquery, createvalue, function (error, results, fields) {
        if (error) {
            console.log("response error", error);
            return res.status(status.error).send(error);
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "status": true, "error": errorMessage });
        } else {
            const dbResponse = results[0];
            return res.status(status.success).send({ "msg": "Submitted successfully", "results": dbResponse, "status": true });
        }
    });

}


const updateCitizenProfileById = async (req, res) => {
    const { id } = req.params;
    const { name_in, mobile_in, district_in, constituency_in, address_in, pincode_in, age_in, gender_in } = req.body;
    const profilequery = 'call updatecitizenprofile(?,?,?,?,?,?,?,?,?);'
    const profilevalue = [id, name_in, mobile_in, district_in, constituency_in, address_in, pincode_in, age_in, gender_in]
    connection.query(profilequery, profilevalue, function (error, results, fields) {
        if (error) {
            console.log("response error", error);
            return res.status(status.error).send(error);
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "status": true, "error": errorMessage });
        } else {
            const dbResponse = results[0];
            console.log(dbResponse);
            return res.status(status.success).send({ "msg": "profile updated successfully", "status": true });
        }
    });

};

export {
    readallvotergrievance,
    readCitizenById,
    readvotergrievance,
    viewCandidate,
    readallsurveyquestionbyID,
    createvotergrievance,
    updateCitizenProfileById
}
