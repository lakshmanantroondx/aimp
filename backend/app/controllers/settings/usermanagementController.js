import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';
import connection from '../../db/dev/pool';
import {isEmpty, empty } from '../../helpers/validations';

import {errorMessage,successMessage,status,} from '../../helpers/status';

/**
   * Create usermanagement
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const createusermanagement = async (req, res) => {
    console.log('testing', req.body)
    const { created_by,email,mobile,name,password,preferred_language,regional_name,user_name,user_type,role_id} = req.body;
    // const created_date = moment(new Date());
    const delete_status=0;
    const createQuery = `insert into user (created_by,email,mobile,name,password,preferred_language,regional_name,user_name,user_type,role_id,delete_status,created_date) 
    values(?,?,?,?,?,?,?,?,?,?,?,current_timestamp) `;
    const values = [
        created_by,email,mobile,name,password,preferred_language,regional_name,user_name,user_type,role_id,delete_status
    ];
    connection.query(createQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exception in user management is", error)
            errorMessage.error = 'Exception in create user management';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        } 
        const dbResponse = results;
        return res.status(status.success).send({ "results": dbResponse, "status": true,"msg":"inserted successfully" });
    });
};

/**
 * Update usermanagement
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated usermanagement
 */

const updateusermanagement = async (req, res) => {
   const { id } = req.params;
   const { updated_by,email,mobile,name,preferred_language,regional_name,user_name,user_type,role_id} = req.body;
    const updateQuery = `update user set
    updated_by=?,email=?,mobile=?,name=?,preferred_language=?,regional_name=?,user_name=?,user_type=?,role_id=?,
    updated_date=current_timestamp
    where id=? `;
    const values = [
        updated_by,email,mobile,name,preferred_language,regional_name,user_name,user_type,role_id,id
    ];
    connection.query(updateQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exceptoin in update user  is", error)
            errorMessage.error = 'Exception in update user ';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse = results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });
    });
};

/**
   * Delete usermanagement
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deleteusermanagement = async (req, res) => {
    const { id } = req.params;
    const values = [id]
    const deleteQuery = 'delete from user where id=?';
    connection.query(deleteQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exceptoin in delete user is", error)
            errorMessage.error = 'Exception in delete deleteuser';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse = results;
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });

};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAllusermanagement = async (req, res) => {
    const getAllQuery = 'select * from user';
    connection.query(getAllQuery, function (error, results, fields) {
        if (error) {
            console.log("Error in read all user is", error)
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse = results;
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });
};

/**
   * Get getusermanagementById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getusermanagementById = async (req, res) => {
    const { id } = req.params;
    const values = [id]
    const getAllQuery = 'select * from user where id=?';
    connection.query(getAllQuery,values, function (error, results, fields) {
        if (error) {
            console.log("Error in read all user is", error)
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse = results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });
};

export {
    createusermanagement,
    getAllusermanagement,
    updateusermanagement,
    deleteusermanagement,
    getusermanagementById
}
