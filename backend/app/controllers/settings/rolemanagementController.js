import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';
import connection from '../../db/dev/pool';
import {isEmpty, empty } from '../../helpers/validations';

import {errorMessage,successMessage,status,} from '../../helpers/status';

/**
   * Create rolemanagement
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const createrolemanagement = async (req, res) => {
    console.log('testing', req.body)
    const { } = req.body;

    const created_on = moment(new Date());

    const createQuery = 'insert into rolemanagement () values() returning *';
    const values = [
    ];

    try {
        const { rows } = await dbQuery.query(createQuery, values);
        const dbResponse = rows[0];
        return res.status(status.created).send(dbResponse);
    } catch (error) {
        if (error.routine === '_bt_check_unique') {
            errorMessage.error = 'Unable to create';
            return res.status(status.conflict).send(errorMessage);
        }
    }
};

/**
 * Update rolemanagement
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated rolemanagement
 */

const updaterolemanagement = async (req, res) => {
   const {  } = req.params;
   const {  } = req.body;
   const {  } = req.user;

   const findQuery = 'select * from rolemanagement where ----';

   const updateQuery = 'update rolemanagement set ---- where ---- ';

   try {
       const { rows } = await dbQuery.query(findQuery, []);
       const dbResponse = rows[0];
       if (!dbResponse) {
            errorMessage.error = 'Not found';
            return res.status(status.notfound).send(errorMessage);
       }
       const values = [

        ];
       const response = await dbQuery.query(updateQuery, values);
       const dbResult = response.rows[0];
       return res.status(status.success).send(dbResult);
   } catch (error) {
       errorMessage.error = 'Operation was not successful';
       return res.status(status.error).send(errorMessage);
   }
};

/**
   * Delete rolemanagement
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deleterolemanagement = async (req, res) => {
    const {  } = req.params;
    const {  } = req.user;
    const deleteQuery = 'delete from rolemanagement where ---- ';
    try {
        const { rows } = await dbQuery.query(deleteQuery, []);
        const dbResponse = rows[0];
        if (!dbResponse) {
            errorMessage.error = 'You have no data with this parm ';
            return res.status(status.notfound).send(errorMessage);
        }
        successMessage.data = {};
        successMessage.data.message = 'Deleted successfully';
        return res.status(status.success).send(successMessage);
    } catch (error) {
        return res.status(status.error).send(error);
    }

};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAllrolemanagement = async (req, res) => {
    const getAllQuery = 'select * from role';
    connection.query(getAllQuery, function (error, results, fields) {
        if (error) {
            console.log("Error in read all user is", error)
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse = results;
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });
};

/**
   * Get getrolemanagementById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getrolemanagementById = async (req, res) => {
    const { id } = req.params;
    const values = [id]
    const getAllQuery = 'select * from role where id=?';
    connection.query(getAllQuery,values, function (error, results, fields) {
        if (error) {
            console.log("Error in read all user is", error)
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse = results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });
};

export {
    createrolemanagement,
    getAllrolemanagement,
    updaterolemanagement,
    deleterolemanagement,
    getrolemanagementById
}
