import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';
import connection from '../../db/dev/pool';

import {isEmpty, empty } from '../../helpers/validations';

import {errorMessage,successMessage,status,} from '../../helpers/status';

/**
   * Create Booth
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const createBooth = async (req, res) => {

    const { created_by,name,regional_name,ward_id,booth_number } = req.body;
    const readboothequery = 'call createbooth(?,?,?,?,?);'
    const readboothvalue = [ created_by,name,regional_name,ward_id,booth_number ]
    connection.query(readboothequery,readboothvalue, function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });


};

/**
 * Update Booth
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated Booth
 */

const updateBooth = async (req, res) => {
   const { id,updated_by,booth_name,regional_name,booth_number,ward_id } = req.body;

   const updatevalues = [ id,updated_by,booth_name,regional_name,booth_number,ward_id ];

   const updateQuery = 'call updatebooth(?,?,?,?,?,?);';
   connection.query(updateQuery,updatevalues, function (error, results, fields) {
    if (error){
        console.log("response error",error);
    return res.status(status.error).send(error);
    } 
    else if(results[0] == 0 ){
        errorMessage.error = 'No data found';
        return res.status(status.error).send({"status": true, "error": errorMessage});
    } else {
        const dbResponse=results[0];
        return res.status(status.success).send({"results": dbResponse, "status": true});
    }
  });

};

/**
   * Delete Booth
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deleteBooth = async (req, res) => {
    const { id } = req.body;
    const readboothequery = 'call deletebooth(?);'
    connection.query(readboothequery,[id], function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });

};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAllBooth = async (req, res) => {
    const readboothequery = 'call readallbooth();'
    connection.query(readboothequery,[], function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });

};

/**
   * Get getBoothById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getBoothById = async (req, res) => {
    const { id } = req.body;
    const readboothequery = 'call readbooth(?);'
    const readboothvalue = [ id ]
    connection.query(readboothequery,readboothvalue, function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });
};

const getBoothByfilter = async (req, res) => {
    const { district_id,constituency_id,ward_id } = req.body;
    const readboothequery = 'call readallboothbyfilter(?,?,?);'
    const readboothvalue = [ district_id,constituency_id,ward_id ]
    connection.query(readboothequery,readboothvalue, function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });
};

const viewagentlist = async(req,res) => {
    const readagentquery = 'call readallagent();'
    connection.query(readagentquery,[], function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });
} 

const viewagentdetails = async(req,res) => {
    const { id } = req.body;
    const readagentquery = 'call readagentdetails(?);';
    connection.query(readagentquery,[id], function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });
} 


const totalvoters = async(req,res) => {
    const { boothid } = req.body;
    const totalvotersquery = 'call totalvotersonboothID(?);';
    connection.query(totalvotersquery,[boothid], function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });
} 


const totalagents = async(req,res) => {
    const totalvotersquery = 'call totalagents();';
    connection.query(totalvotersquery,[], function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });
} 

const masterlist = async(req,res) => {
    const readmasterquery = 'call readallmaster();'
    connection.query(readmasterquery,[], function (error, results, fields) {
        if (error){
            console.log("error in readallmaster",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });
} 

export {
    createBooth,
    getAllBooth,
    updateBooth,
    deleteBooth,
    getBoothById,
    getBoothByfilter,
    viewagentlist,
    masterlist,
    viewagentdetails,
    totalvoters,
    totalagents
}
