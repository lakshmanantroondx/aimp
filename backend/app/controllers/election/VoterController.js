import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';
import connection from '../../db/dev/pool';

import {isEmpty, empty } from '../../helpers/validations';

import {errorMessage,successMessage,status,} from '../../helpers/status';

/**
   * Create Voter
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const createVoter = async (req, res) => {
    const { address, age, constituency_id, created_by, district_id, email_id, gender, guardian_name, having_pan, mobile, name, regional_name, voter_id, ward_id, booth_id } = req.body;
    const readvoterquery = 'call createvoter(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);'
    const readvotervalue = [ address, age, constituency_id, created_by, district_id, email_id, gender, guardian_name, having_pan, mobile, name, regional_name, voter_id, ward_id, booth_id  ]
    connection.query(readvoterquery,readvotervalue, function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });

};

/**
 * Update Voter
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated Voter
 */


const updateVoter = async (req, res) => {
   const { id,address,age,updated_by,email_id,gender,guardian_name,having_pan,mobile,name,regional_name,voter_id } = req.body;

   const updateQuery = 'call updatevoter(?,?,?,?,?,?,?,?,?,?,?,?);';

   const updatevalues = [ id,address,age,updated_by,email_id,gender,guardian_name,having_pan,mobile,name,regional_name,voter_id ];

   connection.query(updateQuery,updatevalues, function (error, results, fields) {
    if (error){
        console.log("response error",error);
    return res.status(status.error).send(error);
    } 
    else if(results[0] == 0 ){
        errorMessage.error = 'No data found';
        return res.status(status.error).send({"status": true, "error": errorMessage});
    } else {
        const dbResponse=results[0];
        return res.status(status.success).send({"results": dbResponse, "status": true});
    }
  });


};

/**
   * Delete Voter
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deleteVoter = async (req, res) => {
    const { id } = req.body;
    const deletequery = 'call deletevoter(?);'
    const deletevalue = [ id ]
    connection.query(deletequery,deletevalue, function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });

};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAllVoter = async (req, res) => {
    const { district_id,constituency_id,ward_id,booth_id } = req.params;
    const readvoterquery = 'call readallvoterbyfilter(?,?,?,?);'
    const readvotervalues = [  district_id,constituency_id,ward_id,booth_id ];
    connection.query(readvoterquery, readvotervalues, function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });
};

/**
   * Get getVoterById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getVoterById = async (req, res) => {
    const { id } = req.body;
    const readvoterquery = 'call readvoter(?);'
    const readvotervalue = [ id ]
    connection.query(readvoterquery,readvotervalue, function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });
};

/**
   * Get getVoterAttendanceById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
  const getVoterAttendanceById = async (req, res) => {
    const { id } = req.params;
    const readvoterquery = 'call readavoterattendancebyid(?);'
    const readvotervalue = [ id ]
    connection.query(readvoterquery,readvotervalue, function (error, results, fields) {
        if (error){
            console.log("error in getVoterAttendanceById",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });
};



const updatevoterpersonal = async (req, res) => {
    const { id,marital_status,religion_id,caste_id,total_familymembers } = req.body;
    const readvoterquery = 'call updatevoterpersonal(?,?,?,?,?);'
    const readvotervalues = [  id,marital_status,religion_id,caste_id,total_familymembers ];
    connection.query(readvoterquery, readvotervalues, function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse,"msg":"updated successfully", "status": true});
        }
      });
};

const updatevotercommunication = async (req, res) => {
    const { id,mobile_number,whatsup_id,email_id,facebook_id,twitter_id } = req.body;
    const updatevoterquery = 'call updatevotercommunication(?,?,?,?,?,?);'
    const updatevotervalues = [  id,mobile_number,whatsup_id,email_id,facebook_id,twitter_id  ];
    connection.query(updatevoterquery, updatevotervalues, function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse,"msg":"updated successfully", "status": true});
        }
      });
};


const updatevoterfinancial = async (req, res) => {
    const { id,employment_type,education_id,profession_id,income_slab_id,house_type,ispan} = req.body;
    const updatevoterquery = 'call updatevoterfinancial(?,?,?,?,?,?,?);'
    const updatevotervalues = [  id,employment_type,education_id,profession_id,income_slab_id,house_type,ispan  ];
    connection.query(updatevoterquery, updatevotervalues, function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse,"msg":"updated successfully", "status": true});
        }
      });
};

const createvoterprediction = async (req, res) => {
    const { party_id,predicted_by,prediction_result,voter_id} = req.body;
    const predictionvoterquery = 'call createvoter_prediction(?,?,?,?);'
    const predictionvotervalues = [  party_id,predicted_by,prediction_result,voter_id  ];
    connection.query(predictionvoterquery, predictionvotervalues, function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse,"msg":"prediction created successfully", "status": true});
        }
      });
};

export {
    createVoter,
    getAllVoter,
    updateVoter,
    deleteVoter,
    getVoterById,
    updatevoterpersonal,
    updatevotercommunication,
    updatevoterfinancial,
    createvoterprediction,
    getVoterAttendanceById
}