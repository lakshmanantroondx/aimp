import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';

import {isEmpty, empty } from '../../helpers/validations';

import {errorMessage,successMessage,status,} from '../../helpers/status';

/**
   * Create BoothExecutiveRegion
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const createBoothExecutiveRegion = async (req, res) => {
    console.log('testing', req.body)
    const { } = req.body;

    const created_on = moment(new Date());

    const createQuery = 'insert into BoothExecutiveRegion () values() returning *';
    const values = [
    ];

    try {
        const { rows } = await dbQuery.query(createQuery, values);
        const dbResponse = rows[0];
        return res.status(status.created).send(dbResponse);
    } catch (error) {
        if (error.routine === '_bt_check_unique') {
            errorMessage.error = 'Unable to create';
            return res.status(status.conflict).send(errorMessage);
        }
    }
};

/**
 * Update BoothExecutiveRegion
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated BoothExecutiveRegion
 */

const updateBoothExecutiveRegion = async (req, res) => {
   const {  } = req.params;
   const {  } = req.body;
   const {  } = req.user;

   const findQuery = 'select * from BoothExecutiveRegion where ----';

   const updateQuery = 'update BoothExecutiveRegion set ---- where ---- ';

   try {
       const { rows } = await dbQuery.query(findQuery, []);
       const dbResponse = rows[0];
       if (!dbResponse) {
            errorMessage.error = 'Not found';
            return res.status(status.notfound).send(errorMessage);
       }
       const values = [

        ];
       const response = await dbQuery.query(updateQuery, values);
       const dbResult = response.rows[0];
       return res.status(status.success).send(dbResult);
   } catch (error) {
       errorMessage.error = 'Operation was not successful';
       return res.status(status.error).send(errorMessage);
   }
};

/**
   * Delete BoothExecutiveRegion
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deleteBoothExecutiveRegion = async (req, res) => {
    const {  } = req.params;
    const {  } = req.user;
    const deleteQuery = 'delete from BoothExecutiveRegion where ---- ';
    try {
        const { rows } = await dbQuery.query(deleteQuery, []);
        const dbResponse = rows[0];
        if (!dbResponse) {
            errorMessage.error = 'You have no data with this parm ';
            return res.status(status.notfound).send(errorMessage);
        }
        successMessage.data = {};
        successMessage.data.message = 'Deleted successfully';
        return res.status(status.success).send(successMessage);
    } catch (error) {
        return res.status(status.error).send(error);
    }

};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAllBoothExecutiveRegion = async (req, res) => {
    const { } = req.user.parms;
    const getAllQuery = 'select * from BoothExecutiveRegion where --- ';
    try {
        const { rows } = await dbQuery.query(getAllQuery, []);
        const dbResponse = rows;

        if (dbResponse[0] === undefined) {
            errorMessage.error = 'No data found';
            return res.status(status.notfound).send(errorMessage);
        }
        return res.status(status.success).send(dbResponse);
    } catch (error) {
        errorMessage.error = 'An error Occured';
        return res.status(status.error).send(errorMessage);
    }
};

/**
   * Get getBoothExecutiveRegionById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getBoothExecutiveRegionById = async (req, res) => {
    const { } = req.user.parms;
    const { } = req.params;

    var getQuery = 'select * from BoothExecutiveRegion where ---- ';

    try {
        const { rows } = await dbQuery.query(getQuery, []);
        const dbResponse = rows;
        if (dbResponse[0] === undefined) {
            errorMessage.error = 'No data found';
            return res.status(status.notfound).send(errorMessage);
        }
        return res.status(status.success).send(dbResponse);
    } catch (error) {
        errorMessage.error = 'An error Occured';
        return res.status(status.error).send(errorMessage);
    }
};

export {
    createBoothExecutiveRegion,
    getAllBoothExecutiveRegion,
    updateBoothExecutiveRegion,
    deleteBoothExecutiveRegion,
    getBoothExecutiveRegionById
}