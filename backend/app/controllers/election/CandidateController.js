import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';
import connection from '../../db/dev/pool';
import {isEmpty, empty } from '../../helpers/validations';

import {errorMessage,successMessage,status,} from '../../helpers/status';

/**
   * Create Candidate
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const createCandidate = async (req, res) => {
    const { name,regional_name,gender,party_id,photo_url,description,regional_description, created_by,candidateManifesto,constituency_id,age,district_id } = req.body;
    const candidateinsertquery = 'INSERT INTO  candidate (name,regional_name,gender,party_id,photo_url,description,regional_description, created_by,constituency_id,age,district_id) VALUES(?,?,?,?,?,?,?,?,?,?,?);';
    const candidatemanifestoinserquery = 'INSERT INTO candidate_manifesto(candidate_id,description,regional_description) values(?,?,?);';
    const candidatevalues = [ name,regional_name,gender,party_id,photo_url,description,regional_description, created_by,constituency_id,age,district_id ];
    connection.query(candidateinsertquery,candidatevalues,function(err, results){
        if(err){
            return res.status(status.error).send(err);
        } else if(results[0] == 0 ) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        }
        else {
            const dbResponse = results[0];
            const candidateid = results.insertId;
            var i;
            for ( i in candidateManifesto){
                const description = candidateManifesto[i].description;
                const regional_description = candidateManifesto[i].regional_description;
                const candidatemanifestovalues = [ candidateid,description,regional_description ];
                connection.query(candidatemanifestoinserquery,candidatemanifestovalues,function(err, results){
                    if(err){
                        return res.status(status.error).send(error);
                    } else if(results[0] == 0 ) {
                        errorMessage.error = 'No data found';
                        return res.status(status.error).send({"status": true, "error": errorMessage});
                    }
                    else {
                    }
                })
            }
            return res.status(status.success).send({"results": dbResponse,"canidateID" : candidateid, "status": true});
        }
    })



};

/**
 * Update Candidate
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated Candidate
 */

const updateCandidate = async (req, res) => {
    const { id,name,regional_name,gender,party_id,photo_url,description,regional_description, update_by,candidateManifesto } = req.body;
    const candidateupdatequery = 'call updatecandidate(?,?,?,?,?,?,?,?,?)';
    const candidatemanifestoinserquery = ' call createcandidateManifesto(?,?,?);';
    const candidateupdatevalues = [ id,name,regional_name,gender,party_id,photo_url,description,regional_description, update_by ];
    const deletecandidatequery = 'call deletecandidateManifesto(?);';
    const deletecandidatevalues = [ id ];
    connection.query(candidateupdatequery,candidateupdatevalues,function(err, results){
        if(err){
            return res.status(status.error).send(err);
        } else if(results[0] == 0 ) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        }
        else {
            const dbResponse = results[0];
            connection.query(deletecandidatequery,deletecandidatevalues,function(err, results){
                if(err){
                    return res.status(status.error).send(error);
                } else if(results[0] == 0 ) {
                    errorMessage.error = 'No data found';
                    return res.status(status.error).send({"status": true, "error": errorMessage});
                }
                else {
                }
            })
            var i;
            for ( i in candidateManifesto){
                const description = candidateManifesto[i].description;
                const regional_description = candidateManifesto[i].regional_description;
                const candidatemanifestovalues = [ id,description,regional_description ];
                connection.query(candidatemanifestoinserquery,candidatemanifestovalues,function(err, results){
                    if(err){
                        return res.status(status.error).send(err);
                    } else if(results[0] == 0 ) {
                        errorMessage.error = 'No data found';
                        return res.status(status.error).send({"status": true, "error": errorMessage});
                    }
                    else {
                    }
                })
            }
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
    })
};

/**
   * Delete Candidate
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deleteCandidate = async (req, res) => {
    const { id } = req.body;
    const deletevalues = [ id ];
    const deleteQuery = 'call deletecandidateManifesto(?);';
    connection.query(deleteQuery,deletevalues, function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });


};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAllCandidate = async (req, res) => {
    const getAllQuery = 'call readallcandidate();';
    connection.query(getAllQuery,[],function(err, results){
        if(err){
            return res.status(status.error).send(error);
        } else if(results[0] == 0 ) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        }
        else {
            const dbResponse = results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
    })
};


const uploadcandidateimage = async(req, res) => {
    var response = { };
    response.path =  '/uploads/candidate/' + req.file.filename ;
    response.filename = req.file.filename;
    response.status = "true";
    res.status(status.success).send(response);
} 

const getcandidatebyID = async (req, res) => {
    const { id } = req.body;
    const getAllQuery = 'call readcandidatebyID(?);';
    const getmanifestoQuery = 'call readcandidatemanifesto(?);';
    connection.query(getAllQuery,[id],function(err, results){
        if(err){
            return res.status(status.error).send(error);
        } else if(results[0] == 0 ) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        }
        else {
            const dbResponse = {};
            dbResponse.data = results[0];
            connection.query(getmanifestoQuery,[id],function(err, results){
                if(err){
                    return res.status(status.error).send(error);
                } else if(results[0] == 0 ) {
                    errorMessage.error = 'No data found';
                    return res.status(status.error).send({"status": true, "error": errorMessage});
                }
                else {
                    const manifesto = results[0];
                    dbResponse.manifesto = manifesto;
                    return res.status(status.success).send({ "results": dbResponse, "status": true });
                    
                }
            })
            //return res.status(status.success).send({"results": dbResponse, "status": true});
        }
    })
};

export {
    createCandidate,
    getAllCandidate,
    updateCandidate,
    deleteCandidate,
    uploadcandidateimage,
    getcandidatebyID
}