import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';
import connection from '../../db/dev/pool';
import {isEmpty, empty } from '../../helpers/validations';

import {errorMessage,successMessage,status,} from '../../helpers/status';

/**
   * Create PartyEvents
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const createPartyEvents = async (req, res) => {
    const { created_by,constituency_id, type,venue, members,description,start_time,end_time } = req.body;
    const createvalues = [ 
        created_by,constituency_id, type,venue, members,description,start_time,end_time
    ];

    const createQuery = 'CALL createpartyevent (?, ?, ?, ?, ?, ?,?,?);';

    connection.query(createQuery,createvalues,function(err, results){
        if(err){
            return res.status(status.error).send(err);
        } else if(results[0] == 0 ) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        }
        else {
            const dbResponse = results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
    })

};

/**
 * Update PartyEvents
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated PartyEvents
 */

const updatePartyEvents = async (req, res) => {
   const { id,updated_by,constituency_id,type,venue,members,description,start_time,end_time } = req.body;
   const updateQuery = 'CALL updatepartyevent (?, ?, ?, ?, ?, ?, ?, ?, ?);';
   const updatevalues = [
    id,updated_by,constituency_id,type,venue,members,description,start_time,end_time
   ];

   connection.query(updateQuery,updatevalues,function(err, results){
    if(err){
        return res.status(status.error).send(err);
    } else if(results[0] == 0 ) {
        errorMessage.error = 'No data found';
        return res.status(status.error).send({"status": true, "error": errorMessage});
    }
    else {
        const dbResponse = results[0];
        return res.status(status.success).send({"results": dbResponse, "status": true});
    }
})

};

/**
   * Delete PartyEvents
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deletePartyEvents = async (req, res) => {
    const { id } = req.params;
    const values = [ id ];
    const deleteQuery = 'call deletepartyevent(?);';
    connection.query(deleteQuery,values,function(err, results){
        if(err){
            return res.status(status.error).send(err);
        } else if(results[0] == 0 ) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        }
        else {
            const dbResponse = results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
    })


};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAllPartyEvents = async (req, res) => {
    const getAllQuery = 'call readallpartyevent();';
    connection.query(getAllQuery,[],function(err, results){
        if(err){
            return res.status(status.error).send(error);
        } else if(results[0] == 0 ) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        }
        else {
            const dbResponse = results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
    })
};

/**
   * Get getPartyEventsById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getPartyEventsById = async (req, res) => {
    const { id } = req.body;
    const values = [ id ];
    const getpartyeventbyidquery = 'call readpartyevent(?);';

    connection.query(getpartyeventbyidquery,values,function(err, results){
        if(err){
            return res.status(status.error).send(err);
        } else if(results[0] == 0 ) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        }
        else {
            const dbResponse = results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
    })

};

export {
    createPartyEvents,
    getAllPartyEvents,
    updatePartyEvents,
    deletePartyEvents,
    getPartyEventsById
}