import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';
import connection from '../../db/dev/pool';


import { isEmpty, empty } from '../../helpers/validations';

import { errorMessage, successMessage, status, } from '../../helpers/status';

/**
   * Create VehicleManagement
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const createVehicleManagement = async (req, res) => {
    const { created_by, created_date, delete_status, constituency_id, district_id, vehicle_name, vehicle_number,
        owner_name, owner_mobile_number, vehicle_colour, vehicle_type, description } = req.body;
    const createVehicleQuery = 'call createvehicle_management(?,?,?,?,?,?,?,?,?,?);'
    const createVehicleValue = [created_by, constituency_id, district_id, vehicle_name, vehicle_number,
        owner_name, owner_mobile_number, vehicle_colour, vehicle_type, description]
    connection.query(createVehicleQuery, createVehicleValue, function (error, results, fields) {
        if (error) {
            console.log("response error", error);
            return res.status(status.error).send(error);
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "status": true, "error": errorMessage });
        } else {
            const dbResponse = results[0];
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }
    });
};

/**
 * Update VehicleManagement
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated VehicleManagement
 */

const updateVehicleManagement = async (req, res) => {
    const { id } = req.params
    const { updated_by_in, constituency_id_in, district_id_in, vehicle_name_in, vehicle_number_in, owner_name_in, owner_mobile_number_in, vehicle_colour_in, vehicle_type_in, description_in } = req.body;
    const values = [id, updated_by_in, constituency_id_in, district_id_in, vehicle_name_in, vehicle_number_in, owner_name_in, owner_mobile_number_in, vehicle_colour_in, vehicle_type_in, description_in];
    const updateQuery = 'call updatevehicle_management(?,?,?,?,?,?,?,?,?,?,?)';
    connection.query(updateQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exception in update vehicle management  is", error)
            errorMessage.error = 'Exception in update vehicle management';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse = results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });
    });
};

/**
   * Delete VehicleManagement
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deleteVehicleManagement = async (req, res) => {
    const { id } = req.params;
    const values = [id]

    const deleteQuery = 'call deletevehicle_management(?)';

    connection.query(deleteQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exceptoin in delete vehicle management is", error)
            errorMessage.error = 'Exception in  vehicle management';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse = results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });

};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAllVehicleManagement = async (req, res) => {
    const getAllQuery = 'call readallvehicle_management()';
    connection.query(getAllQuery, function (error, results, fields) {
        if (error) {
            console.log("Error in read all vehicle management is", error)
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse = results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });
};

/**
   * Get getVehicleManagementById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getVehicleManagementById = async (req, res) => {
    const { id } = req.params;
    const values = [id]
    const getQuery = 'call readvehicle_management(?)';

    connection.query(getQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Error in read vehicle management by id is", error)
            errorMessage.error = 'Exception in vehicle management by id';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else {
            const dbResponse = results[0];
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }

    });
};

/**
   * Get getVehicleByFilter
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
  const getVehicleByFilter = async (req, res) => {
    const { district_id, constituency_id } = req.params;
    const values = [district_id, constituency_id]

    const getQuery = 'call readallvehicle_management_byfilter(?,?)';

    connection.query(getQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Error in read all vehicle management by filter is", error)
            errorMessage.error = 'Exception in vehicle management by filter';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else {
            const dbResponse = results[0];
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }

    });
};


export {
    createVehicleManagement,
    getAllVehicleManagement,
    updateVehicleManagement,
    deleteVehicleManagement,
    getVehicleManagementById,
    getVehicleByFilter
}