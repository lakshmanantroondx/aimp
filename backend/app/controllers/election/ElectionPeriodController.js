import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';
import connection from '../../db/dev/pool';
import {isEmpty, empty } from '../../helpers/validations';

import {errorMessage,successMessage,status,} from '../../helpers/status';

/**
   * Create ElectionPeriod
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */
const createElectionPeriod = async (req, res) => {
    const { created_by,election_period,start_date,end_date,electionphase } = req.body;
    const creatEPQuery = 'INSERT INTO  election_period (created_by,created_date,election_period,start_date,end_date) VALUES(?,current_timestamp,?,?,?);';
    const createEPhasequery = 'INSERT INTO election_phase(created_by,election_period_id,election_date,canvas_closing_date) VALUES (?,?,?,?);';
    const values = [
        created_by,election_period,start_date,end_date
    ];
    connection.query(creatEPQuery,values,function(err, results){
        if(err){
            return res.status(status.error).send(err);
        } else if(results[0] == 0 ) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        }
        else {
            const dbResponse = results;
            const election_period_id = results.insertId;
            var i;
            for (i in electionphase){
                const election_date = electionphase[i].election_date;
                const canvas_closing_date = electionphase[i].canvas_closing_date;
                const phasevalues = [ created_by,election_period_id,election_date,canvas_closing_date ];
                connection.query(createEPhasequery,phasevalues, function(err, results){
                    if (err){
                        console.log("response error",err);
                    }
                    else if(results[0] == 0 ){
                        errorMessage.error = 'No data found';
                    } else {
                        //const dbResponse=results[0];                   
                     }

                })
            }
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
    })

};




/**
   * Delete ElectionPeriod
   * @param {object} req
   * @param {object} res
   * @returns {void}
   */

const deleteElectionPeriod = async (req, res) => {
    const { id } = req.body;
    const deleteQuery = 'call deleteelectionperiod(?);';
    connection.query(deleteQuery,[id], function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        }
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });

};


/**
   * Get All
   * @param {object} req
   * @param {object} res
   * @returns {object}
   */
const getAllElectionPeriod = async (req, res) => {
    const getAllQuery = 'call readallelection_period();';
    connection.query(getAllQuery,[], function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        }
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });

};

/**
   * Get getElectionPeriodById
   * @param {object} req
   * @param {object} res
   * @returns {object}
   */
const getElectionPeriodById = async (req, res) => {
    const { id } = req.body;
    var getQuery = 'call readelection_period(?);';
    connection.query(getQuery,[id], function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        }
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });
};

const updateElectionPeriodbyID = async (req, res) => {
    const { id,updated_by,election_period,start_date,end_date,electionphase  } = req.body;
    const updatevalues = [ id,updated_by,election_period,start_date,end_date ];
    const updateQuery = 'call updateelection_period(?,?,?,?,?);';
    const deletequery = 'call deleteelectionphase(?);';
    const deleteperiodquery = 'call deleteelectionperiod(?);';
    const createEPhasequery = 'INSERT INTO election_phase(election_period_id,election_date,canvas_closing_date) VALUES (?,?,?);';
    connection.query(updateQuery,updatevalues, function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        }
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            connection.query(deletequery,[id], function (error, results, fields) {
                if (error){
                return res.status(status.error).send(error);
                }
                else if(results[0] == 0 ){
                } else {
                   // const dbResponse=results[0];
                }
              });
              var i;
              for (i in electionphase){
                  const election_date = electionphase[i].election_date;
                  const canvas_closing_date = electionphase[i].canvas_closing_date;
                  const phasevalues = [ id,election_date,canvas_closing_date ];
                  connection.query(createEPhasequery,phasevalues, function(err, results){
                      if (err){
                          console.log("response error",err);
                      }
                      else if(results[0] == 0 ){
                          errorMessage.error = 'No data found';
                      } else {
                          //const dbResponse=results[0];                   
                       }
                  })
              }
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });

};

export {
    createElectionPeriod,
    getAllElectionPeriod,
    deleteElectionPeriod,
    getElectionPeriodById,
    updateElectionPeriodbyID
}
