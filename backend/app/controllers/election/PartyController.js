import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';
import connection from '../../db/dev/pool';

import {isEmpty, empty } from '../../helpers/validations';

import {errorMessage,successMessage,status,} from '../../helpers/status';

/**
   * Create Party
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

   //name regional name is owner party image leader image  created by created date


const createParty = async (req, res) => {
    const { name,regionalname,is_owner,partyimage,leaderimage,created_by} = req.body;

    const createQuery = 'call createparty(?,?,?,?,?,?);';
    const values = [
        name,regionalname,is_owner,partyimage,leaderimage,created_by
    ];

    connection.query(createQuery,values,function(err, results){
        if(err){
            return res.status(status.error).send(error);
        } else if(results[0] == 0 ) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        }
        else {
            const dbResponse = results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
    })

};

/**
 * Update Party
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated Party
 */

const updateParty = async (req, res) => {
   const { id,updated_by,partyname,regionalname,partyimage,leaderimage,is_owner } = req.body;

   const updateQuery = 'call updateparty(?,?,?,?,?,?,?);';

   const updatevalues = [ id,updated_by,partyname,regionalname,partyimage,leaderimage,is_owner ];
   connection.query(updateQuery,updatevalues,function(err, results){
    if(err){
        return res.status(status.error).send(err);
    } else if(results[0] == 0 ) {
        errorMessage.error = 'No data found';
        return res.status(status.error).send({"status": true, "error": errorMessage});
    }
    
    else {
        const dbResponse = results[0];
        return res.status(status.success).send({"results": dbResponse, "status": true});
    }
})

};

/**
   * Delete Party
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deleteParty = async (req, res) => {
    const { id } = req.body;
    const deleteQuery = 'call deleteparty(?);';
    const values = [ id ];
    connection.query(deleteQuery,values,function(err, results){
        if(err){
            return res.status(status.error).send(error);
        } else if(results[0] == 0 ) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        }
        
        else {
            const dbResponse = results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
    })


};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAllParty = async (req, res) => {
    const getAllQuery = 'call readallparty();';
    connection.query(getAllQuery,[],function(err, results){
        if(err){
            return res.status(status.error).send(error);
        } else if(results[0] == 0 ) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        }
        
        else {
            const dbResponse = results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
    })
};

/**
   * Get getPartyById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getPartyById = async (req, res) => {
        const { id } = req.body;

    var getQuery = 'call readparty(?);';
    connection.query(getQuery,[id],function(err, results){
        if(err){
            return res.status(status.error).send(error);
        } else if(results[0] == 0 ) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        }
        else {
            const dbResponse = results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
    })
};
const uploadleaderimage = async(req, res) => {
    var response = { };
    response.path =  '/uploads/leader/' + req.file.filename ;
    response.filename = req.file.filename;
    response.status = "true";
    res.status(status.success).send(response);
} 

const uploadpartyimage = async(req, res) => {
    var response = { };
    response.path =  '/uploads/party/' + req.file.filename ;
    response.filename = req.file.filename;
    response.status = "true";
    res.status(status.success).send(response);
} 
export {
    createParty,
    getAllParty,
    updateParty,
    deleteParty,
    getPartyById,
    uploadleaderimage,
    uploadpartyimage
}