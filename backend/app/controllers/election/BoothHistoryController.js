import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';
import connection from '../../db/dev/pool';

import {isEmpty, empty } from '../../helpers/validations';

import {errorMessage,successMessage,status,} from '../../helpers/status';

/**
   * Create BoothHistory
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const createBoothHistory = async (req, res) => {
    const { booth_name,candidate_name,election_period_id,party_id,total_voters,support_count,constituency_id,created_by } = req.body;
    const createboothQuery = 'call createBoothHistory(?,?,?,?,?,?,?,?);';
    const boothvalues = [
        booth_name,candidate_name,election_period_id,party_id,total_voters,support_count,constituency_id,created_by
    ];
    connection.query(createboothQuery,boothvalues,function(err, results){
        if(err){
            return res.status(status.error).send(error);
        }
        else {
            const dbResponse = results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
    })

};

/**
 * Update BoothHistory
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated BoothHistory
 */

const updateBoothHistory = async (req, res) => {
   const {  id,booth_name,candidate_name,election_period_id,party_id,total_voters,support_count,updated_by,constituency_id } = req.body;

   const updateQuery = 'call updateboothHistory(?,?,?,?,?,?,?,?,?);';
   const updatevalues = [
    id,booth_name,candidate_name,election_period_id,party_id,total_voters,support_count,updated_by,constituency_id
   ];

   connection.query(updateQuery,updatevalues,function(err, results){
    if(err){
        return res.status(status.error).send(err);
    }else if (results[0] == 0){
        errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
    }
    else {
        const dbResponse = results[0];
        return res.status(status.success).send({"results": dbResponse, "status": true});
    }
})

};

/**
   * Delete BoothHistory
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deleteBoothHistory = async (req, res) => {
    const { id } = req.body;
    const deletevalues = [ id ];
    const deleteQuery = 'call deleboothHistory(?);';
    connection.query(deleteQuery,deletevalues, function (error, results, fields) {
        if (error){
            console.log("response error",error);
        return res.status(status.error).send(error);
        } 
        else if(results[0] == 0 ){
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        } else {
            const dbResponse=results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
      });

};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAllBoothHistory = async (req, res) => {
    const getAllQuery = 'call readallBoothHistory();';
    connection.query(getAllQuery,[],function(err, results){
        if(err){
            return res.status(status.error).send(error);
        } else if(results[0] == 0 ) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        }
        else {
            const dbResponse = results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
    })
};

/**
   * Get getBoothHistoryById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getBoothHistoryById = async (req, res) => {
    const { id } = req.body;
    const getbyIDQuery = 'call readBoothHistorybyID(?);';
    connection.query(getbyIDQuery,[id],function(err, results){
        if(err){
            return res.status(status.error).send(error);
        } else if(results[0] == 0 ) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        }
        else {
            const dbResponse = results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
    })
};


const getboothhistorybyfilter = async (req, res) => {
    const { district_id,constituency_id } = req.body;
    const getbyIDQuery = 'call readallbooth_historybyfilter(?,?);';
    connection.query(getbyIDQuery,[district_id,constituency_id],function(err, results){
        if(err){
            return res.status(status.error).send(error);
        } else if(results[0] == 0 ) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({"status": true, "error": errorMessage});
        }
        else {
            const dbResponse = results[0];
            return res.status(status.success).send({"results": dbResponse, "status": true});
        }
    })
};

export {
    createBoothHistory,
    getAllBoothHistory,
    updateBoothHistory,
    deleteBoothHistory,
    getBoothHistoryById,
    getboothhistorybyfilter
}