import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';
import connection from '../../db/dev/pool'
const bcrypt = require('bcrypt');
const saltRounds = 10;

import { isEmpty, empty, comparePassword, generateToken } from '../../helpers/validations';

import { errorMessage, successMessage, status, } from '../../helpers/status';

/**
 * Create login
 * @param {object} req
 * @param {object} res
 * @returns {object} reflection object
 */

const surveylogin = async (req, res) => {
    const { user_name, password } = req.body;
    const selectquery = "select * from survey_user where user_name = ?";
    const values = [user_name];
    var token;

    connection.query(selectquery, values, async function (error, results, fields) {
        if (error) {
            console.log("Error in survey login", error)
            errorMessage.error = 'No data found';
            return res.status(status.success).send({ "errorMessage": "No User Found", "status": false });
        }
        if (results[0] == undefined) {
            return res.status(status.success).send({ "errorMessage": "No User Found", "status": false });
        }
        var cmpPass = await comparePassword(password, results[0].password);
        if (cmpPass) {
            console.log("comparing password");
            var response = results[0];
            var tokenparm = {
                id: response.id,
                email: response.email,
                user_name: response.user_name,
                name: response.name
            };
            token = await generateToken(tokenparm);
            //console.log("token ->",token);
        } else {

            return res.status(status.success).send({ "errorMessage": "password incorrect", "status": false });

        }
        return res.status(status.success).send({ "data": results, "token": token, "status": true });

    });
};


/**
 * Create login
 * @param {object} req
 * @param {object} res
 * @returns {object} reflection object
 */

 const usercreate =async (req,res)=>{
    const password = req.body.password;
    const encryptedPassword = await bcrypt.hash(password, saltRounds)
    var users={
       "email":req.body.email,
       "password":encryptedPassword,
       "name":req.body.name
     }
    //  console.log(users)
    
    connection.query('INSERT INTO users SET ?',users, function (error, results, fields) {
         if (error) {
             console.log(error)
            errorMessage.error = 'error ocurred';
            return res.status(status.error).send({ "message": "error ocurred", "status": false });
        }
        else{
            return res.status(status.success).send({ "message": "user registered sucessfully", "status": true });
        }
    });
 }

const userlogin = async (req, res) => {
    var email= req.body.email;
    var password = req.body.password;
    connection.query('SELECT * FROM users WHERE email = ?',[email], async function (error, results, fields) {
      if (error) {
        res.send({
          "code":400,
          "failed":"error ocurred"
        })
      }else{
        if(results.length >0){
          const comparision = await bcrypt.compare(password, results[0].password)
          if(comparision){
              res.send({
                "code":200,
                "success":"login sucessfull",
                "result":results[0]
              })
          }
          else{
            res.send({
                 "code":204,
                 "success":"Email and password does not match"
            })
          }
        }
        else{
          res.send({
            "code":206,
            "success":"Email does not exits"
              });
        }
      }
      });
    // const { UserName, Password } = req.body;
    // const selectquery = `select u.*,ua.district_id,ua.constituency_id,ua.ward_id,ua.booth_id, 
    // d.name as "DistrictName",c.name as "ConstituencyName",w.name as "WardName",b.name as "BoothName",
    //  d.regional_name as "DistrictRegional", c.regional_name as "ConstituencyRegional",
    //   w.regional_name as "WardRegional", b.regional_name as "BoothRegional",
    //   d.district_number,c.constituency_number,w.ward_number,b.booth_number 
    // from user u 
    // left join user_association ua on ua.user_id=u.id left join district d on ua.district_id=d.id left join constituency c on ua.constituency_id=c.id left join ward w on ua.ward_id=w.id left join booth b on ua.booth_id=b.id where u.user_name=?`;
    // //const selectquery = 'call userlogin(?)';
    // const values = [UserName];
    // var token;

    // connection.query(selectquery, values, async function (error, results, fields) {
    //     if (error) {
    //         console.log("Error in login", error)
    //         errorMessage.error = 'No data found';
    //         return res.status(status.success).send({ "errorMessage": "No User Found", "status": false });
    //     }
    //     if (results[0] == undefined) {
    //         return res.status(status.success).send({ "errorMessage": "No User Found", "status": false });
    //     }
    //     const { UserName, Password } = req.body;

    //     //var cmpPass = await comparePassword(req.body.Password, results[0].password);
    //     if (Password === results[0].password) {
    //         console.log("comparing password");
    //         var response = results[0];
    //         var tokenparm = {
    //             id: response.id,
    //             email: response.email,
    //             user_name: response.user_name,
    //             name: response.name
    //         };
    //         token = await generateToken(tokenparm);
    //         //console.log("token ->",token);
    //     } else {

    //         return res.status(status.success).send({ "errorMessage": "password incorrect", "status": false });

    //     }
    //     return res.status(status.success).send({ "data": results, "token": token, "status": true });

    // });
};

/**
   * Create user
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const createuser = async (req, res) => {
    console.log('testing', req.body)
    const { } = req.body;

    const created_on = moment(new Date());

    const createQuery = 'insert into user () values() returning *';
    const values = [
    ];

    try {
        const { rows } = await dbQuery.query(createQuery, values);
        const dbResponse = rows[0];
        return res.status(status.created).send(dbResponse);
    } catch (error) {
        if (error.routine === '_bt_check_unique') {
            errorMessage.error = 'Unable to create';
            return res.status(status.conflict).send(errorMessage);
        }
    }
};

/**
 * Update user
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated user
 */

const updateuser = async (req, res) => {
    const { } = req.params;
    const { } = req.body;
    const { } = req.user;

    const findQuery = 'select * from user where ----';

    const updateQuery = 'update user set ---- where ---- ';

    try {
        const { rows } = await dbQuery.query(findQuery, []);
        const dbResponse = rows[0];
        if (!dbResponse) {
            errorMessage.error = 'Not found';
            return res.status(status.notfound).send(errorMessage);
        }
        const values = [

        ];
        const response = await dbQuery.query(updateQuery, values);
        const dbResult = response.rows[0];
        return res.status(status.success).send(dbResult);
    } catch (error) {
        errorMessage.error = 'Operation was not successful';
        return res.status(status.error).send(errorMessage);
    }
};

/**
   * Delete user
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deleteuser = async (req, res) => {
    const { } = req.params;
    const { } = req.user;
    const deleteQuery = 'delete from user where ---- ';
    try {
        const { rows } = await dbQuery.query(deleteQuery, []);
        const dbResponse = rows[0];
        if (!dbResponse) {
            errorMessage.error = 'You have no data with this parm ';
            return res.status(status.notfound).send(errorMessage);
        }
        successMessage.data = {};
        successMessage.data.message = 'Deleted successfully';
        return res.status(status.success).send(successMessage);
    } catch (error) {
        return res.status(status.error).send(error);
    }

};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAlluser = async (req, res) => {
    const { } = req.user.parms;
    const getAllQuery = 'select * from user where --- ';
    try {
        const { rows } = await dbQuery.query(getAllQuery, []);
        const dbResponse = rows;

        if (dbResponse[0] === undefined) {
            errorMessage.error = 'No data found';
            return res.status(status.notfound).send(errorMessage);
        }
        return res.status(status.success).send(dbResponse);
    } catch (error) {
        errorMessage.error = 'An error Occured';
        return res.status(status.error).send(errorMessage);
    }
};

/**
   * Get getuserById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getuserById = async (req, res) => {
    const { } = req.user.parms;
    const { } = req.params;

    var getQuery = 'select * from user where ---- ';

    try {
        const { rows } = await dbQuery.query(getQuery, []);
        const dbResponse = rows;
        if (dbResponse[0] === undefined) {
            errorMessage.error = 'No data found';
            return res.status(status.notfound).send(errorMessage);
        }
        return res.status(status.success).send(dbResponse);
    } catch (error) {
        errorMessage.error = 'An error Occured';
        return res.status(status.error).send(errorMessage);
    }
};

/**
   * Create userassociation
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const createuserassociation = async (req, res) => {
    const { created_by, user_id, district_id, constituency_id, ward_id, booth_id } = req.body;
    const values = [created_by, user_id, district_id, constituency_id, ward_id, booth_id];
    const createAssoctionQuery = 'call createuserassociation(?,?,?,?,?,?)';

    connection.query(createAssoctionQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exception in create user association is", error)
            errorMessage.error = 'Exception in create user association';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        return res.status(status.success).send({ "results": results, "status": true });

    });
};

/**
   * Create citizen
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const createCitizen = async (req, res) => {
    const { name_in, email_id_in } = req.body;
    const values = [name_in, email_id_in];
    const selectvalue = [email_id_in];
    const selectQuery = 'call readgeneralvoterbymailid(?)';

    connection.query(selectQuery, selectvalue, function (error, results, fields) {
        const dbResponse=results[0];
        console.log("response is ",dbResponse);
        if (error) {
            console.log("Error in select citizen  by email id is", error)
            errorMessage.error = 'Exception in select citizen by email id';
            return res.status(status.error).send({ "ErrorMessage": errorMessage, "status": false });
        }
        else if (dbResponse.length===0) {
            const createCitizenQuery = 'call createcitizen(?,?)';
            connection.query(createCitizenQuery, values, function (error, results, fields) {
                const dbResponse=results[0];
                if (error) {
                    console.log("Exception in create citizen is", error)
                    errorMessage.error = 'Exception in create citizen association';
                    return res.status(status.error).send({ "results": errorMessage, "status": false });
                }
                return res.status(status.success).send({ "results": dbResponse, "status": true });

            });
        }
        else {
            // const dbResponse = results[0];
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }

    });

};

/**
   * Get getCitizenByMailId
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getCitizenByMailId = async (req, res) => {
    const { id } = req.params;
    const values = [id]
    const getQuery = 'call readCitizenByMailId(?)';

    connection.query(getQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Error in read citizen  by id is", error)
            errorMessage.error = 'Exception in read citizen by id';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.success).send({ "results": errorMessage, "status": false });
        }
        else {
            const dbResponse = results[0];
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }

    });
};



/**
   * Get getUserAssociationByUserId
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getUserAssociationByUserId = async (req, res) => {
    const { id } = req.params;
    const values = [id]
    const getQuery = 'call readuserassociation(?)';

    connection.query(getQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Error in read user association by id is", error)
            errorMessage.error = 'Exception in user association by id';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.success).send({ "results": errorMessage, "status": false });
        }
        else {
            const dbResponse = results[0];
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }

    });
};

/**
 * updateUserAssociation
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated VehicleManagement
 */

const updateUserAssociation = async (req, res) => {
    const { id } = req.params
    const { updated_by_in, district_id_in, constituency_id_in, ward_id_in, booth_id_in } = req.body;
    const values = [id, updated_by_in, district_id_in, constituency_id_in, ward_id_in, booth_id_in];
    const updateQuery = 'call updateuserassociation(?,?,?,?,?,?)';
    connection.query(updateQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exception in update user association  is", error)
            errorMessage.error = 'Exception in update user association';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse = results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });
    });
};


export {
    createuser,
    getAlluser,
    updateuser,
    deleteuser,
    getuserById,
    surveylogin,
    userlogin,
    createuserassociation,
    getUserAssociationByUserId,
    updateUserAssociation,
    createCitizen,
    usercreate
}
