import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';
import connection from '../../db/dev/pool';
const readXlsxFile = require('read-excel-file/node');
const excel = require("exceljs");
import { isEmpty, empty } from '../../helpers/validations';

import { errorMessage, successMessage, status, } from '../../helpers/status';

/**
   * Create constituency
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const createconstituency = async (req, res) => {
    const { created_by, name, regional_name, district_id, image_url, constituency_number } = req.body;
    const values = [created_by, name, regional_name, district_id, image_url, constituency_number];
    const createQuery = 'call createconstituency(?,?,?,?,?,?)';

    connection.query(createQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exception in create constituency is", error)
            errorMessage.error = 'Exception in create constituency';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse = results;
        return res.status(status.success).send({ "results": results, "status": true });

    });
};

/**
 * Update constituency
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated constituency
 */

const updateconstituency = async (req, res) => {
    const { id } = req.params
    const { updated_by_in, image_url_in, constituency_name_in, regional_name_in, constituency_number_in, district_id_in } = req.body;
    const values = [id, updated_by_in, image_url_in, constituency_name_in, regional_name_in, constituency_number_in, district_id_in];
    const createQuery = 'call updateconstituency(?,?,?,?,?,?,?)';
    connection.query(createQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exceptoin in update constituency  is", error)
            errorMessage.error = 'Exception in update constituency ';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse = results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });
    });
};

/**
   * Delete constituency
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deleteconstituency = async (req, res) => {
    const { id } = req.params;
    const values = [id]

    const deleteQuery = 'call deleteconstituency(?)';

    connection.query(deleteQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exceptoin in delete constituency is", error)
            errorMessage.error = 'Exception in delete constituency';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse = results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });

};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAllconstituency = async (req, res) => {
    const getAllQuery = 'call readallconstituency()';
    connection.query(getAllQuery, function (error, results, fields) {
        if (error) {
            console.log("Error in read all constituency is", error)
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse = results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });
};

/**
   * Get getconstituencyById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getconstituencyById = async (req, res) => {
    const { id } = req.params;
    const values = [id]

    const getQuery = 'call readconstituency(?)';

    connection.query(getQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Error in read constituency is", error)
            errorMessage.error = 'Exception in getdistrictById';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else {
            const dbResponse = results[0];
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }

    });
};

/**
   * Get getconstituencyByDistrictId
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
  const getconstituencyByDistrictId = async (req, res) => {
    const { id } = req.params;
    const values = [id]

    const getQuery = 'call readallconstituencybydistrict(?)';

    connection.query(getQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Error in read all constituency by district is", error)
            errorMessage.error = 'Exception in readallconstituencybydistrict';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else {
            const dbResponse = results[0];
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }

    });
};

const uploadimage = async (req, res) => {
    var response = {};
    response.path = '/uploads/constituency/' + req.file.filename;
    response.filename = req.file.filename;
    response.status = "true";
    res.status(status.success).send(response);
}
const bulkupload = async (req, res) => {
    const { created_by } = req.body;
    importExcelData2MySQL(__basedir + '/uploads/bulkupload/' + req.file.filename,req.body);
    res.json({
        'msg': 'File uploaded/import successfully!', 'file': req.file.filename,status:true
    });
}
function importExcelData2MySQL(filePath,data) {
    readXlsxFile(filePath).then((rows) => {
        rows.shift();
        let tempresults;
        let finalresults=[];
        rows.forEach((row) => {
            var values = [row[3]];
            var getQuery = 'select id from district where name=?';
            connection.query(getQuery, values, (err, rows) => {
              if(err) throw err;
              tempresults = {
                name: row[0],
                regionalname: row[1],
                constituency_number: row[2],
                districtname: row[3],
                district_id:rows[0].id,
                created_by:data.created_by,
                delete_status:false
            };
              setValue(tempresults);
            });
        }); 
    })
}
function setValue(data) {
    const values = [data.created_by, data.delete_status,data.name, data.regionalname, data.district_id, data.constituency_number];
    const createQuery = `INSERT INTO  constituency(created_by, delete_status,name, regional_name, district_id, constituency_number) 
    VALUES(?,?,?,?,?,?);`;
    connection.query(createQuery, values, function (error, results, fields) {
        if (error)  
        console.log(error)
        const dbResponse = results;
        console.log(results)
    });
  }

  const uploadBulkbooth = async (req, res) => {
    const { created_by } = req.body;
    importExcelData2MySQLBooth(__basedir + '/uploads/bulkupload/' + req.file.filename,req.body);
    res.json({
        'msg': 'File uploaded/import successfully!', 'file': req.file.filename,status:true
    });
}
function importExcelData2MySQLBooth(filePath,data) {
    readXlsxFile(filePath).then((rows) => {
        rows.shift();
        let tempresults;
        rows.forEach((row) => {
              tempresults = {
                name: row[0],
                regional_name: row[1],
                booth_number: row[2],
                ward_id: row[3],
                created_by:data.created_by,
                delete_status:false
            };
              setValueBooth(tempresults);
        }); 
    })
}
function setValueBooth(data) {
    const values = [data.created_by, data.delete_status,data.name, data.regional_name, data.booth_number, data.ward_id];
    const createQuery = `INSERT INTO  booth(created_by, delete_status,name, regional_name, booth_number, ward_id) 
    VALUES(?,?,?,?,?,?);`;
    connection.query(createQuery, values, function (error, results, fields) {
        if (error)  
        console.log(error)
        const dbResponse = results;
        console.log(results)
    });
  }

  const uploadBulkvoter = async (req, res) => {
    const { created_by,constituency_id,ward_id,booth_id,district_id } = req.body;
    importExcelData2MySQLvoter(__basedir + '/uploads/bulkupload/' + req.file.filename,req.body);
    res.json({
        'msg': 'File uploaded/import successfully!', 'file': req.file.filename,status:true
    });
}
function importExcelData2MySQLvoter(filePath,data) {
    readXlsxFile(filePath).then((rows) => {
        rows.shift();
        let tempresults;
        rows.forEach((row) => {
              tempresults = {
                name: row[0],
                guardian_name: row[1],
                regional_name: row[2],
                voter_id: row[3],
                age:row[4],
                gender:row[5],
                mobile:row[6],
                email_id:row[7],
                address:row[8],
                created_by:data.created_by,
                constituency_id:data.constituency_id,
                ward_id:data.ward_id,
                booth_id:data.booth_id,
                district_id:data.district_id,
                alive:1,
                having_pan:0,
                voter_status:"Pending"
            };
              setValuevoter(tempresults);
        }); 
    })
}
function setValuevoter(data) {
    const values = [data.district_id, data.booth_id,data.ward_id, data.constituency_id, data.created_by, data.address,data.email_id,data.mobile,data.gender,data.age,data.voter_id,data.regional_name,data.guardian_name,data.name,data.alive,data.having_pan,data.voter_status];
    const createQuery = `INSERT INTO  voter(district_id, booth_id,ward_id, constituency_id, created_by, address,email_id,mobile,gender,age,voter_id,regional_name,guardian_name,name,alive,having_pan,voter_status) 
    VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);`;
    connection.query(createQuery, values, function (error, results, fields) {
        if (error)  
        console.log(error)
        const dbResponse = results;
        console.log(results)
    });
  }

  const uploadBulkboothhis = async (req, res) => {
    const { created_by } = req.body;
    importExcelData2MySQLBoothhis(__basedir + '/uploads/bulkupload/' + req.file.filename,req.body);
    res.json({
        'msg': 'File uploaded/import successfully!', 'file': req.file.filename,status:true
    });
}
function importExcelData2MySQLBoothhis(filePath,data) {
    readXlsxFile(filePath).then((rows) => {
        rows.shift();
        let tempresults;
        rows.forEach((row) => {
              tempresults = {
                booth_name: row[0],
                candidate_name: row[1],
                total_voters: row[2],
                support_count: row[3],
                party_id: row[4],
                election_period_id: row[5],
                created_by:data.created_by,
            };
              setValueBoothhis(tempresults);
        }); 
    })
}
function setValueBoothhis(data) {
    const values = [data.created_by, data.election_period_id,data.party_id, data.support_count, data.total_voters, data.candidate_name,data.booth_name];
    const createQuery = `INSERT INTO  booth_election_history(created_by, election_period_id,party_id, support_count, total_voters, candidate_name,booth_name) 
    VALUES(?,?,?,?,?,?,?);`;
    connection.query(createQuery, values, function (error, results, fields) {
        if (error)  
        console.log(error)
        const dbResponse = results;
        console.log(results)
    });
  }


  const downloadboothHisTemplate = async (req, res) => {
    const { party_id,election_period_id } = req.params;
    console.log("testing", req.params)
    let workbook = new excel.Workbook();
    let worksheet = workbook.addWorksheet("BoothElectionHistoryList");
    let tutorials = [];
    worksheet.columns = [
      { header: "Booth Name", key: "boothname", width: 25 },
      { header: "Candidate Name", key: "candidatename", width: 25 },
      { header: "Total Voters", key: "totalvoters", width: 25 },
      { header: "Support count", key: "supportcount", width: 25 },
      { header: "Party Name", key: "partyname", width: 25 },
      { header: "Election Period", key: "electionperiod", width: 25 },
    ];
    worksheet.getCell('E2').value = req.params.party_id; 
    worksheet.getCell('F2').value = req.params.election_period_id; 
    // Add Array Rows
    worksheet.addRows(tutorials);
    // res is a Stream object
    res.setHeader(
      "Content-Type",
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    );
    res.setHeader(
      "Content-Disposition",
      "attachment; filename=" + "BoothElectionHistoryList.xlsx"
    );
    return workbook.xlsx.write(res).then(function () {
      res.status(200).end();
    });
  }

  const downloadboothTemplate = async (req, res) => {
    const { constituency_id,ward_id } = req.params;
    console.log("testing", req.params)
    let workbook = new excel.Workbook();
    let worksheet = workbook.addWorksheet("BoothList");
    let tutorials = [];
    worksheet.columns = [
      { header: "Name", key: "boothname", width: 25 },
      { header: "Regional Name", key: "regionalname", width: 25 },
      { header: "Booth Number", key: "boothnumber", width: 25 },
      { header: "ward details", key: "warddetails", width: 25 },
      { header: "constituency details", key: "constituencydetails", width: 25 },
    ];
    worksheet.getCell('D2').value = req.params.ward_id; 
    worksheet.getCell('E2').value = req.params.constituency_id; 
    // Add Array Rows
    worksheet.addRows(tutorials);
    // res is a Stream object
    res.setHeader(
      "Content-Type",
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    );
    res.setHeader(
      "Content-Disposition",
      "attachment; filename=" + "BoothList.xlsx"
    );
    return workbook.xlsx.write(res).then(function () {
      res.status(200).end();
    });
  }

  const downloadVoterTemplate = async (req, res) => {
    let workbook = new excel.Workbook();
    let worksheet = workbook.addWorksheet("VoterList");
    let tutorials = [];
    worksheet.columns = [
      { header: "Name", key: "name", width: 20 },
      { header: "GuardianName", key: "guardianname", width: 20 },
      { header: "Regional Name", key: "regionalname", width: 25 },
      { header: "VoterId", key: "VoterId", width: 25 },
      { header: "Age", key: "age", width: 5 },
      { header: "Gender", key: "gender", width: 10 },
      { header: "Mobile", key: "mobile", width: 15 },
      { header: "Emailid", key: "email", width: 20 },
      { header: "Address", key: "address", width: 25 },
    ];
    worksheet.getCell('F2').dataValidation = {
        type: 'list',
        allowBlank: true,
        formulae: ['"MALE,FEMALE,OTHERS"']
      };
    // Add Array Rows
    worksheet.addRows(tutorials);
    // res is a Stream object
    res.setHeader(
      "Content-Type",
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    );
    res.setHeader(
      "Content-Disposition",
      "attachment; filename=" + "VoterList.xlsx"
    );
    return workbook.xlsx.write(res).then(function () {
      res.status(200).end();
    });
  }

export {
    createconstituency,
    getAllconstituency,
    updateconstituency,
    deleteconstituency,
    getconstituencyById,
    uploadimage,
    bulkupload,
    getconstituencyByDistrictId,
    downloadboothHisTemplate,
    downloadboothTemplate,
    uploadBulkbooth,
    downloadVoterTemplate,
    uploadBulkvoter,
    uploadBulkboothhis
}