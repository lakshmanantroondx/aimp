import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';
import connection from '../../db/dev/pool';
import { isEmpty, empty } from '../../helpers/validations';
import { errorMessage, successMessage, status, } from '../../helpers/status';

/**
   * Create grievancesubcategory
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const creategrievancesubcategory = async (req, res) => {
    const { created_by, name, regional_name, constituency_id_in, grievance_category_id_in } = req.body;
    const values = [created_by, name, regional_name, constituency_id_in, grievance_category_id_in];
    const createQuery = 'call creategrievancesubcategory(?,?,?,?,?)';

    connection.query(createQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exception in create grievance sub category is", error)
            errorMessage.error = 'Exception in create grievance sub category';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse = results;
        return res.status(status.success).send({ "results": results, "status": true });

    });
};

/**
 * Update grievancesubcategory
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated grievancesubcategory
 */

const updategrievancesubcategory = async (req, res) => {
    const { id } = req.params
    const { updated_by_in, category_name_in, regional_name_in, constituency_id_in, grievance_category_id_in } = req.body;
    const values = [id, updated_by_in, category_name_in, regional_name_in, constituency_id_in, grievance_category_id_in];
    const createQuery = 'call updategrievancesubcategory(?,?,?,?,?,?)';
    connection.query(createQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exceptoin in update grievance sub category is", error)
            errorMessage.error = 'Exception in update grievance sub category';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse=results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });
    });
};

/**
   * Delete grievancesubcategory
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deletegrievancesubcategory = async (req, res) => {
    const { id } = req.params;
    const values = [id]
    const deleteQuery = 'call deletegrievancesubcategory(?)';

    connection.query(deleteQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exceptoin in delete grievance sub category is", error)
            errorMessage.error = 'Exception in delete grievance sub category';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse=results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });

};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAllgrievancesubcategory = async (req, res) => {
    const getAllQuery = 'call readallgrievancesubcategory()';
    connection.query(getAllQuery, function (error, results, fields) {
        if (error) {
            console.log("Error in read all grievance sub category is", error)
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse=results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });
};

/**
   * Get getgrievancesubcategoryById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getgrievancesubcategoryById = async (req, res) => {
    const { id } = req.params;
    const values = [id]
    const getQuery = 'call readgrievancesubcategory(?)';

    connection.query(getQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Error in reading grievance sub category  is", error)
            errorMessage.error = 'Exception in read grievance sub category';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else {
            const dbResponse=results[0];
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }

    });
};

export {
    creategrievancesubcategory,
    getAllgrievancesubcategory,
    updategrievancesubcategory,
    deletegrievancesubcategory,
    getgrievancesubcategoryById
}
