import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';
import connection from '../../db/dev/pool';


import { isEmpty, empty } from '../../helpers/validations';

import { errorMessage, successMessage, status, } from '../../helpers/status';

/**
   * Create district
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const createdistrict = async (req, res) => {
    const { created_by, name, regional_name, district_number } = req.body;
    const values = [created_by, name, regional_name, district_number];
    const createQuery = 'call createdistirct(?,?,?,?)';

    connection.query(createQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exceptoin in create district is", error)
            errorMessage.error = 'Exception in create disrict';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse=results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });

};

/**
 * Update district
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated district
 */

const updatedistrict = async (req, res) => {
    const{id}= req.params
    const { updated_by_in, district_name_in, regional_name_in, district_number_in } = req.body;
    const values = [id,updated_by_in,district_name_in, regional_name_in, district_number_in];
    const createQuery = 'call updatedistirct(?,?,?,?,?)';
    connection.query(createQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exceptoin in update district is", error)
            errorMessage.error = 'Exception in update disrict';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse=results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });
    });
};

/**
   * Delete district
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deletedistrict = async (req, res) => {
    const { id } = req.params;
    const values = [id]

    const deleteQuery = 'call deletedistrict(?)';

    connection.query(deleteQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exceptoin in delete district is", error)
            errorMessage.error = 'Exception in delete disrict';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse=results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });

};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAlldistrict = async (req, res) => {

    const getAllQuery = 'call readalldistirct';
    connection.query(getAllQuery, function (error, results, fields) {
        if (error) {
            console.log("Error in read all district is", error)
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse=results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });
};

/**
   * Get getdistrictById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getdistrictById = async (req, res) => {
    const { id } = req.params;
    const values = [id]

    const getQuery = 'call readdistirct(?)';

    connection.query(getQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Error in read district is", error)
            errorMessage.error = 'Exception in getdistrictById';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else {
            const dbResponse=results[0];
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }

    });
};

export {
    createdistrict,
    getAlldistrict,
    updatedistrict,
    deletedistrict,
    getdistrictById
}
