import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';
import connection from '../../db/dev/pool';
import { isEmpty, empty } from '../../helpers/validations';
const readXlsxFile = require('read-excel-file/node');
import { errorMessage, successMessage, status, } from '../../helpers/status';

/**
   * Create ward
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const createward = async (req, res) => {
    const { created_by, name, regional_name, ward_number, constituency_id } = req.body;
    const values = [created_by, name, regional_name, ward_number, constituency_id];
    const createQuery = 'call createward(?,?,?,?,?)';

    connection.query(createQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exception in ward constituency is", error)
            errorMessage.error = 'Exception in create ward';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        } 
        const dbResponse = results;
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });
};

/**
 * Update ward
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated ward
 */

const updateward = async (req, res) => {
    const { id } = req.params
    const { updated_by_in, ward_name_in, regional_name_in, ward_number_in, constituency_id_in } = req.body;
    const values = [id, updated_by_in, ward_name_in, regional_name_in, ward_number_in, constituency_id_in];
    const createQuery = 'call updateward(?,?,?,?,?,?)';
    connection.query(createQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exceptoin in updateward is", error)
            errorMessage.error = 'Exception in update ward';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse = results;
        return res.status(status.success).send({ "results": dbResponse, "status": true });
    });
};

/**
   * Delete ward
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deleteward = async (req, res) => {
    const { id } = req.params;
    const values = [id]

    const deleteQuery = 'call deleteward(?)';

    connection.query(deleteQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exceptoin in delete ward is", error)
            errorMessage.error = 'Exception in delete deleteward';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse = results;
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });

};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAllward = async (req, res) => {
    const getAllQuery = 'call readallward()';
    connection.query(getAllQuery, function (error, results, fields) {
        if (error) {
            console.log("Error in read all ward is", error)
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse = results;
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });
};

/**
   * Get getwardById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getwardById = async (req, res) => {
    const { id } = req.params;
    const values = [id]

    const getQuery = 'call readward(?)';

    connection.query(getQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Error in reading ward  is", error)
            errorMessage.error = 'Exception in readward';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else {
            const dbResponse = results;
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }

    });
};

/**
   * Get getwardByFilterId
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
  const getwardByFilterId = async (req, res) => {
    const { districtId,constituencyId } = req.params;
    const values = [districtId, constituencyId]

    const getQuery = 'call readallwardbyfilter(?,?)';

    connection.query(getQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Error in getwardByFilterId  is", error)
            errorMessage.error = 'Exception in getwardByFilterId';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else {
            const dbResponse = results;
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }

    });
};



const wardbulkupload = async (req, res) => {
    const { created_by,constituency_id } = req.body;
    importExcelData2MySQL(__basedir + '/uploads/bulkupload/' + req.file.filename,req.body);
    res.json({
        'msg': 'File uploaded/import successfully!', 'file': req.file.filename,status:true
    });
}

function importExcelData2MySQL(filePath,data) {
    readXlsxFile(filePath).then((rows) => {
        rows.shift();
        let tempresults;
        let finalresults=[];
        rows.forEach((row) => {
              tempresults = {
                name: row[0],
                regional_name: row[1],
                ward_number: row[2],
                constituency_id:data.constituency_id,
                created_by:data.created_by,
                delete_status:false
            };
            finalresults.push(tempresults)
            setValue(tempresults);
        }); 
        console.log(finalresults)
    })
    
}

function setValue(data) {
    const values = [data.created_by, data.delete_status,data.name, data.regional_name, data.constituency_id, data.ward_number];
    const createQuery = `INSERT INTO  ward(created_by, delete_status,name, regional_name, constituency_id, ward_number) 
    VALUES(?,?,?,?,?,?);`;
    connection.query(createQuery, values, function (error, results, fields) {
        if (error)  
        console.log(error)
        const dbResponse = results;
        console.log(results)
    });
  }

export {
    createward,
    getAllward,
    updateward,
    deleteward,
    getwardById,
    wardbulkupload,
    getwardByFilterId
}
