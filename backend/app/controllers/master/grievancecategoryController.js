import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';
import connection from '../../db/dev/pool';
import { isEmpty, empty } from '../../helpers/validations';
import { errorMessage, successMessage, status, } from '../../helpers/status';

/**
   * Create grievancecategory
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const creategrievancecategory = async (req, res) => {
    const { created_by, name, regional_name, constituency_id_in, type_in } = req.body;
    const values = [created_by, name, regional_name, constituency_id_in, type_in];
    const createQuery = 'call creategrievancecategory(?,?,?,?,?)';

    connection.query(createQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exception in create grievance category is", error)
            errorMessage.error = 'Exception in create grievance category';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse = results;
        return res.status(status.success).send({ "results": results, "status": true });

    });
};

/**
 * Update grievancecategory
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated grievancecategory
 */

const updategrievancecategory = async (req, res) => {
    const { id } = req.params
    const { updated_by_in, category_name_in, regional_name_in, constituency_id_in,type_in } = req.body;
    const values = [id, updated_by_in, category_name_in, regional_name_in, constituency_id_in,type_in];
    const createQuery = 'call updategrievancecategory(?,?,?,?,?,?)';
    connection.query(createQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exceptoin in update grievance category is", error)
            errorMessage.error = 'Exception in update grievance category';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse=results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });
    });
};

/**
   * Delete grievancecategory
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deletegrievancecategory = async (req, res) => {
    const { id } = req.params;
    const values = [id]
    const deleteQuery = 'call deletegrievancecategory(?)';

    connection.query(deleteQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exceptoin in delete grievance category is", error)
            errorMessage.error = 'Exception in delete grievance category';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse=results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });

};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAllgrievancecategory = async (req, res) => {
    const getAllQuery = 'call readallgrievancecategory()';
    connection.query(getAllQuery, function (error, results, fields) {
        if (error) {
            console.log("Error in read all grievance category is", error)
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse=results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });
};

/**
   * Get getgrievancecategoryById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getgrievancecategoryById = async (req, res) => {
    const { id } = req.params;
    const values = [id]
    const getQuery = 'call readgrievancecategory(?)';

    connection.query(getQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Error in reading grievance category  is", error)
            errorMessage.error = 'Exception in read grievance category';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else {
            const dbResponse=results[0];
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }

    });
};

export {
    creategrievancecategory,
    getAllgrievancecategory,
    updategrievancecategory,
    deletegrievancecategory,
    getgrievancecategoryById
}
