import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';
import connection from '../../db/dev/pool';
import {isEmpty, empty } from '../../helpers/validations';

import {errorMessage,successMessage,status,} from '../../helpers/status';

/**
   * Create caste
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const createcaste = async (req, res) => {
    const { created_by, name, regional_name, religion_id, community } = req.body;
    const values = [created_by, name, regional_name, religion_id, community];
    const createQuery = 'call createcaste(?,?,?,?,?)';

    connection.query(createQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exception in create caste is", error)
            errorMessage.error = 'Exception in create caste';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        } 
        const dbResponse=results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });
};

/**
 * Update caste
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated caste
 */

const updatecaste = async (req, res) => {
    const { id } = req.params
    const { updated_by_in, caste_name_in, regional_name_in, religion_id_in, community_in } = req.body;
    const values = [id, updated_by_in, caste_name_in, regional_name_in, religion_id_in, community_in];
    const createQuery = 'call updatecaste(?,?,?,?,?,?)';
    connection.query(createQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exceptoin in update caste is", error)
            errorMessage.error = 'Exception in update caste';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse=results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });
    });
};

/**
   * Delete caste
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deletecaste = async (req, res) => {
    const { id } = req.params;
    const values = [id]

    const deleteQuery = 'call deletecaste(?)';

    connection.query(deleteQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exceptoin in delete caste is", error)
            errorMessage.error = 'Exception in delete delete caste';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse=results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });

};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAllcaste = async (req, res) => {
    const getAllQuery = 'call readallcaste()';
    connection.query(getAllQuery, function (error, results, fields) {
        if (error) {
            console.log("Error in read all caste is", error)
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse=results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });
};

/**
   * Get getcasteById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getcasteById = async (req, res) => {
    const { id } = req.params;
    const values = [id]

    const getQuery = 'call readcaste(?)';

    connection.query(getQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Error in reading caste  is", error)
            errorMessage.error = 'Exception in readcaste';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else {
            const dbResponse=results[0];
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }

    });
};

export {
    createcaste,
    getAllcaste,
    updatecaste,
    deletecaste,
    getcasteById
}
