import moment from 'moment';
import dbQuery from '../../db/dev/dbQuery';
import connection from '../../db/dev/pool';
import {isEmpty, empty } from '../../helpers/validations';
import {errorMessage,successMessage,status,} from '../../helpers/status';

/**
   * Create religion
   * @param {object} req
   * @param {object} res
   * @returns {object} reflection object
   */

const createreligion = async (req, res) => {
    const { created_by, name, regional_name } = req.body;
    const values = [created_by, name, regional_name];
    const createQuery = 'call createreligion(?,?,?)';

    connection.query(createQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exception in create religion is", error)
            errorMessage.error = 'Exception in create religion';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        } 
        const dbResponse = results;
        return res.status(status.success).send({ "results": results, "status": true });

    });
};

/**
 * Update religion
 * @param {object} req 
 * @param {object} res 
 * @returns {object} updated religion
 */

const updatereligion = async (req, res) => {
    const { id } = req.params
    const { updated_by_in, religion_name_in, regional_name_in } = req.body;
    const values = [id, updated_by_in, religion_name_in, regional_name_in];
    const createQuery = 'call updatereligion(?,?,?,?)';
    connection.query(createQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exceptoin in update religion is", error)
            errorMessage.error = 'Exception in update religion';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse=results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });
    });
};

/**
   * Delete religion
   * @param {object} req 
   * @param {object} res 
   * @returns {void} 
   */

const deletereligion = async (req, res) => {
    const { id } = req.params;
    const values = [id]

    const deleteQuery = 'call deletereligion(?)';

    connection.query(deleteQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Exceptoin in delete religion is", error)
            errorMessage.error = 'Exception in delete religion';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse=results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });

};


/**
   * Get All 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getAllreligion = async (req, res) => {
    const getAllQuery = 'call readallreligion()';
    connection.query(getAllQuery, function (error, results, fields) {
        if (error) {
            console.log("Error in read all religion is", error)
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        const dbResponse=results[0];
        return res.status(status.success).send({ "results": dbResponse, "status": true });

    });
};

/**
   * Get getreligionById
   * @param {object} req 
   * @param {object} res 
   * @returns {object} 
   */
const getreligionById = async (req, res) => {
    const { id } = req.params;
    const values = [id]
    const getQuery = 'call readreligion(?)';

    connection.query(getQuery, values, function (error, results, fields) {
        if (error) {
            console.log("Error in reading religion  is", error)
            errorMessage.error = 'Exception in read religion';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else if (results[0] == 0) {
            errorMessage.error = 'No data found';
            return res.status(status.error).send({ "results": errorMessage, "status": false });
        }
        else {
            const dbResponse=results[0];
            return res.status(status.success).send({ "results": dbResponse, "status": true });
        }

    });
};

export {
    createreligion,
    getAllreligion,
    updatereligion,
    deletereligion,
    getreligionById
}
