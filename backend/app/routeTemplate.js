//app/routes/${i}Route.js

"import express from 'express';"

import {
create${i},
getAll${i},
update${i},
delete${i},
get${i}ById } 
"from '../../controllers/${m}/${i}Controller';"

"import verifyAuth from '../../middleware/verifyAuth';"

"const router = express.Router()";


"/**"
"* @typedef Create${i}"
"* @property {string} var1.required - username - eg: test1"
"* @property {string} var2.required - Org Name- eg: test2"
"* @property {string} var3.required - password eg: test3"
"* @property {string} var4.required -Role- eg: test4"
"* @property {string} var5.required - usermail - eg:admin@testing.com"
"* @security JWT"
"*/"
"/**"
"* This function comment is parsed by TroonDx"
"* @route POST /create${i}"
"* @group ${i} API Layer - Operations on ${i}"
"* @param {Create${i}.model} Create${i}.body.required - Create ${i}"
"* @returns {object} 200 - An array of user info"
"* @returns {Error}  default - Unexpected error"
"*/"

"router.post('/create${i}', verifyAuth, create${i});"


"/**"
"* This function comment is parsed by TroonDx"
"* @route GET /getAll${i}"
"* @group ${i} API Layer - Operations on ${i}"
"* @returns {object} 200 - An array of user info"
"* @returns {Error}  default - Unexpected error"
"* @security JWT"
"*/"
"router.get('/getAll${i}',verifyAuth,  getAll${i});"

"/**"
"* @typedef UpdatebyId${i}"
"* @property {string} var1.required - username - eg: test1"
"* @property {string} var2.required - Org Name- eg: test2"
"* @property {string} var3.required - password eg: test3"
"* @property {string} var4.required -Role- eg: test4"
"* @property {string} var5.required - usermail - eg:admin@testing.com"
"*/"
"/**"
"* This function comment is parsed by TroonDx"
"* @route POST /updateById${i}"
"* @group ${i} API Layer - Update Operations on ${i}"
"* @param {UpdatebyId${i}.model} UpdatebyId${i}.body.required - UpdatebyID ${i}"
"* @returns {object} 200 - An array of user info"
"* @returns {Error}  default - Unexpected error"
"* @security JWT"
"*/"
"router.post('/updateById${i}', verifyAuth, update${i});"

"/**"
"* @typedef DeleteById${i}"
"* @property {string} id.required - id - eg: 1"
"*/"
"/**"
"* This function comment is parsed by TroonDx"
"* @route POST /deleteById${i}"
"* @group ${i} API Layer - Delete Operations on ${i}"
"* @param {DeleteById${i}.model} DeleteById${i}.body.required - DeleteById ${i}"
"* @returns {object} 200 - An array of user info"
"* @returns {Error}  default - Unexpected error"
"* @security JWT"
"*/"
"router.post('/deleteById${i}', verifyAuth, delete${i});"

"/**"
"* @typedef Get${i}ById"
"* @property {string} var1.required - username - eg: test1"
"* @property {string} var2.required - Org Name- eg: test2"
"* @property {string} var3.required - password eg: test3"
"* @property {string} var4.required -Role- eg: test4"
"* @property {string} var5.required - usermail - eg:admin@testing.com"
"*/"
"/**"
"* This function comment is parsed by TroonDx"
"* @route POST /get${i}ById"
"* @group ${i} API Layer - Get Operations on ${i}"
"* @param {Get${i}ById.model} Get${i}ById.body.required - Get${i}ById ${i}"
"* @returns {object} 200 - An array of user info"
"* @returns {Error}  default - Unexpected error"
"* @security JWT"
"*/"
"router.get('/get${i}ById/:id', verifyAuth,get${i}ById);"

"export default router;"
