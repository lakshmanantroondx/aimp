sudo apt update
sudo apt install postgresql postgresql-contrib
sudo -i -u postgres
psql

If all is well, then from command terminal give this command to create database & the schema

npm install babel-register babel-preset-env --save-dev

create .env file and set these

DATABASE_URL=postgres://apple:postgres@localhost:5432/healthdirect
PORT=5000
secret=sadfashoifhaosfhsd

create .babelrc file and set these

{
    "presets": ["env"]
}

create config.js and set these

require('babel-register')({
    presets: [ 'env' ]
})
// Import the rest of our application.
module.exports = require('./server.js')