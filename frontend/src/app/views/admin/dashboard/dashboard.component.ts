import { Component, OnInit } from "@angular/core";
import { CommonService } from '../../../service/common.service';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
})
export class DashboardComponent implements OnInit {
  data: {Result:""};
  CovidResult: any;
  constructor(private fb: FormBuilder,private commonService: CommonService) {
    this.bulkUploadForm= this.fb.group({
      file: new FormControl('', [Validators.required]),
    fileSource: new FormControl('', [Validators.required]),
    })
  }
  
  bulkUploadForm:FormGroup;
  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.bulkUploadForm.patchValue({
        fileSource: file
      });
    }else{
      alert("upload file !!!")
    }
  }

  onSubmitbulkUploadForm(val){
    const formData = new FormData();
    formData.append('file', this.bulkUploadForm.get('fileSource').value);
    this.commonService.bulkUpload(formData).subscribe((data: any) => {
     this.CovidResult=data.Result;
    });
  }

  ngOnInit() {}

  imageObject = [{
    image: 'assets/img/slider/covid.jpeg',
    thumbImage: 'assets/img/slider/covid.jpeg',
    title: 'Covid Detection'
  }, {
    image: 'assets/img/slider/Mask_Group_58.jpg',
    thumbImage: 'assets/img/slider/Mask_Group_58.jpg',
    title: 'Aadhar Detection'
  },
  {
    image: 'assets/img/slider/Mask_Group_56.jpg',
    thumbImage: 'assets/img/slider/Mask_Group_56.jpg',
    title: 'Pancard Detection'
  }, {
    image: 'assets/img/slider/vehicle.jpeg',
    thumbImage: 'assets/img/slider/vehicle.jpeg',
    title: 'Car Number Detection '
  }];
}
