import { Component, OnInit } from "@angular/core";
import { CommonService } from '../../../service/common.service';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-pan',
  templateUrl: './pan.component.html',
  styleUrls: ['./pan.component.css']
})
export class PanComponent implements OnInit {
  PanReportRes: boolean;
  PanResName: any;
  PanResNo: any;
  PanResDOB: any;
  PanResultJsonRes: string;
  imageSrc: string;
  constructor(private fb: FormBuilder, private commonService: CommonService) {
    this.bulkUploadFormPan = this.fb.group({
      filePan: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required]),
    })
   }
   bulkUploadFormPan: FormGroup;
  ngOnInit(): void {
  }

  onFileChangePan(event) {
    const reader = new FileReader();
    const [file] = event.target.files;
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imageSrc = reader.result as string;
    }
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.bulkUploadFormPan.patchValue({
        fileSource: file
      });
    } else {
      alert("upload file !!!")
    }
  }

  onSubmitbulkUploadFormPan(val) {
      const formData = new FormData();
      formData.append('file', this.bulkUploadFormPan.get('fileSource').value);
      this.commonService.UploadPan(formData).subscribe((data: any) => {
     
        this.PanReportRes = true;
        this.PanResName = data.Name;
        this.PanResNo = data.PanCardNumber;
        this.PanResDOB = data.DateofBrith;
        this.PanResultJsonRes = JSON.stringify(data);
      });
    
  }

}
