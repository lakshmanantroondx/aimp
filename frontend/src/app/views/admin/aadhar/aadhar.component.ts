import { Component, OnInit } from "@angular/core";
import { CommonService } from '../../../service/common.service';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-aadhar',
  templateUrl: './aadhar.component.html',
  styleUrls: ['./aadhar.component.css']
})
export class AadharComponent implements OnInit {
  AadharReportRes: boolean;
  AadharResName: any;
  AadharResNo: any;
  AadharResDOB: any;
  AadharResultJsonRes: string;

  constructor(private fb: FormBuilder, private commonService: CommonService) {
    this.bulkUploadFormAadhar = this.fb.group({
      fileAadhar: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required]),
    })
   }
   bulkUploadFormAadhar: FormGroup;
   imageSrc: string;
  ngOnInit(): void {
  }
  onFileChangeAadhar(event) {
    const reader = new FileReader();
    const [file] = event.target.files;
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imageSrc = reader.result as string;
    }
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.bulkUploadFormAadhar.patchValue({
        fileSource: file
      });
    } else {
      alert("upload file !!!")
    }
  }

  onSubmitbulkUploadFormAadhar(val) {
   
      const formData = new FormData();
      formData.append('file', this.bulkUploadFormAadhar.get('fileSource').value);
      this.commonService.UploadAadhar(formData).subscribe((data: any) => {
        this.AadharReportRes = true;
        this.AadharResName = data.Name;
        this.AadharResNo = data.AadharNumber;
        this.AadharResDOB = data.DateofBirth;
        this.AadharResultJsonRes = JSON.stringify(data);
      });
    
  }

}
