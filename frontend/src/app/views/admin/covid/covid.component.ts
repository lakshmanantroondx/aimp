import { Component, OnInit } from "@angular/core";
import { CommonService } from '../../../service/common.service';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-covid',
  templateUrl: './covid.component.html',
  styleUrls: ['./covid.component.css']
})
export class CovidComponent implements OnInit {
  CovidReportScore: boolean;
  CovidResult: any;
  CovidScore: any;
  CovidResultJsonRes: string;

  constructor(private fb: FormBuilder, private commonService: CommonService) {
    this.bulkUploadForm = this.fb.group({
      file: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required]),
    })
  }
  imageSrc: string;
  bulkUploadForm: FormGroup;
  ngOnInit(): void {
  }

  onFileChange(event) {
    debugger
    const reader = new FileReader();
    const [file] = event.target.files;
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imageSrc = reader.result as string;
    }
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.bulkUploadForm.patchValue({
        fileSource: file
      });
    } else {
      alert("upload file !!!")
    }
  }

  onSubmitbulkUploadForm(val) {
      const formData = new FormData();
      formData.append('file', this.bulkUploadForm.get('fileSource').value);
      this.commonService.bulkUpload(formData).subscribe((data: any) => {
        this.CovidReportScore = true;
        this.CovidResult = data.Result;
        this.CovidScore = data.Score;
        this.CovidResultJsonRes = JSON.stringify(data);
      });
    
  }

}
