import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarnoplateComponent } from './carnoplate.component';

describe('CarnoplateComponent', () => {
  let component: CarnoplateComponent;
  let fixture: ComponentFixture<CarnoplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarnoplateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarnoplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
