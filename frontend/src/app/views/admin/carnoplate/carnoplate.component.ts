import { Component, OnInit } from "@angular/core";
import { CommonService } from '../../../service/common.service';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-carnoplate',
  templateUrl: './carnoplate.component.html',
  styleUrls: ['./carnoplate.component.css']
})
export class CarnoplateComponent implements OnInit {
  CarnoReportRes: boolean;
  CarnoRes: string;
  CarnoResultJsonRes: string;

  constructor(private fb: FormBuilder, private commonService: CommonService) {
    this.bulkUploadFormCarno = this.fb.group({
      fileCarno: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required]),
    })
  }
  imageSrc: string;
  bulkUploadFormCarno: FormGroup;
  ngOnInit(): void {
  }

  onFileChangeCarno(event) {
    const reader = new FileReader();
    const [file] = event.target.files;
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imageSrc = reader.result as string;
    }
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.bulkUploadFormCarno.patchValue({
        fileSource: file
      });
    } else {
      alert("upload file !!!")
    }
  }

  onSubmitbulkUploadFormCarno(val) {
      const formData = new FormData();
      formData.append('file', this.bulkUploadFormCarno.get('fileSource').value);
      this.commonService.UploadCarno(formData).subscribe((data: any) => {
        if (data.Success === "True") {
          this.CarnoReportRes = true;
          debugger
          // this.CarnoRes = "Car Number :" + data.Result;
          this.CarnoRes = "Car Number :" + data.Result;
          this.CarnoResultJsonRes = JSON.stringify(data);
        }
        else {
          this.CarnoReportRes = true;
          this.CarnoRes = data.message;
          this.CarnoResultJsonRes = JSON.stringify(data);
        }

      });
    
  }

}
