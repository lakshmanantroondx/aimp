import { Component, OnInit } from "@angular/core";
import { CommonService } from '../../../service/common.service';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
})
export class RegisterComponent implements OnInit {
  Result: any;
  constructor(private fb: FormBuilder,private commonService: CommonService,private router: Router, private toastr: ToastrService) {
    this.userCreateForm= this.fb.group({
      name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
    })
  }

  userCreateForm:FormGroup;

  ngOnInit(): void {}

  onSubmituserCreateForm(val){
    // debugger
    // var data={
    //   "email":this.userCreateForm.get('email').value,
    //   "password":this.userCreateForm.get('password').value,
    //   "name":this.userCreateForm.get('name').value
    // }
    var paramdata={
      "email":val.email,
      "password":val.password,
      "name":val.name
    }
    console.log(paramdata);
    this.commonService.usercreate(paramdata).subscribe((data: any) => {
     this.Result=data;
     if(data.status==true){
      this.toastr.success("","user created successfully",{closeButton:true,timeOut:3000});
      this.router.navigate(['auth/login'])
     }
     else{
      this.toastr.error("","please check credentials",{closeButton:true,timeOut:3000});
     }
    });
  }
}
