import { Component, OnInit } from "@angular/core";
import { CommonService } from '../../../service/common.service';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
})
export class LoginComponent implements OnInit {
  Result: any;
  constructor(private fb: FormBuilder,private commonService: CommonService,private router: Router, private toastr: ToastrService) {
    this.userCreateForm= this.fb.group({
      name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
    })
  }

  userCreateForm:FormGroup;

  ngOnInit(): void {}

  onSubmituserCreateForm(val){
    // debugger
    var paramdata={
      "email":val.email,
      "password":val.password
    }
    console.log(paramdata);
    this.commonService.userlogin(paramdata).subscribe((data: any) => {
     this.Result=data;
     if(data.code==200){
      this.toastr.success("","Login successfully",{closeButton:true,timeOut:3000});
      this.router.navigate(['admin/dashboard'])
     }
     else{
      this.toastr.error("",data.success,{closeButton:true,timeOut:3000});
     }
    });
  }
}
