import { Component, OnInit } from "@angular/core";
import { CommonService } from '../../service/common.service';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

@Component({
  selector: "app-index",
  templateUrl: "./index.component.html",
})
export class IndexComponent implements OnInit {
  CovidResult: any; 
  imageClickValue: any;
  CovidReportDiv = false;
  CovidReportScore = false;
  AadharDiv = false;
  AadharReportRes = false;
  CovidScore: any;
  AadharRes: any;
  AadharResName: any;
  AadharResNo: any;
  AadharResDOB: any;
  imageSrc: string;
  PanCardDiv = false;
  PanReportRes: boolean;
  PanResName: any;
  PanResNo: any;
  PanResDOB: any;
  CarnoDiv = false;
  CarnoReportRes: boolean;
  CarnoRes: any;
  CovidResultJsonRes: any;
  ModelHeaderTxt = false;
  AadharResultJsonRes: string;
  PanResultJsonRes: string;
  CarnoResultJsonRes: string;
  PassportDiv = false;
  PassportReportRes = false;
  PassportResultJsonRes: string;
  passportRes: any;
  PassportRes: any;
  ChequeDiv = false;
  ChequeResultJsonRes: string;
  ChequeRes: any;
  ChequeReportRes= false;
  DrivingLicDiv = false;
  VoterDiv = false;
  DrivingLicReportRes= false;
  DrivingLicRes: any;
  DrivingLicResultJsonRes: string;
  VoterReportRes= false;
  VoterRes: any;
  VoterResultJsonRes: string;
  constructor(private fb: FormBuilder, private commonService: CommonService) {
    this.bulkUploadForm = this.fb.group({
      file: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required]),
    })
    this.bulkUploadFormAadhar = this.fb.group({
      fileAadhar: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required]),
    })
    this.bulkUploadFormPan = this.fb.group({
      filePan: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required]),
    })
    this.bulkUploadFormCarno = this.fb.group({
      fileCarno: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required]),
    })
    this.bulkUploadFormPassport = this.fb.group({
      filePassport: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required]),
    })
    this.bulkUploadFormCheque = this.fb.group({
      fileCheque: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required]),
    })
  }
  bulkUploadForm: FormGroup;
  bulkUploadFormAadhar: FormGroup;
  bulkUploadFormPan: FormGroup;
  bulkUploadFormCarno: FormGroup;
  bulkUploadFormPassport: FormGroup;
  bulkUploadFormCheque: FormGroup;
  imageObject = [{
    image: 'assets/img/slider/covid.jpeg',
    thumbImage: 'assets/img/slider/covid.jpeg',
    title: 'Covid Detection'
  }, {
    image: 'assets/img/slider/Mask_Group_58.jpg',
    thumbImage: 'assets/img/slider/Mask_Group_58.jpg',
    title: 'Aadhar Detection'
  },
  {
    image: 'assets/img/slider/Mask_Group_56.jpg',
    thumbImage: 'assets/img/slider/Mask_Group_56.jpg',
    title: 'Pancard Detection'
  }, {
    image: 'assets/img/slider/vehicle.jpeg',
    thumbImage: 'assets/img/slider/vehicle.jpeg',
    title: 'Car Number Detection '
  }, {
    image: 'assets/img/slider/passport.jpeg',
    thumbImage: 'assets/img/slider/passport.jpeg',
    title: 'Passport Extraction'
  }, {
    image: 'assets/img/slider/cheque.jpg',
    thumbImage: 'assets/img/slider/cheque.jpg',
    title: 'Cheque Number Extraction'
  }, {
    image: 'assets/img/slider/drivinglicense.jpg',
    thumbImage: 'assets/img/slider/drivinglicense.jpg',
    title: 'Driving License Extraction'
  }, {
    image: 'assets/img/slider/voterid.jpeg',
    thumbImage: 'assets/img/slider/voterid.jpeg',
    title: 'Voter ID Extraction'
  }];
  ngOnInit(): void { }
  imageClickHandler(e) {
    this.imageClickValue = e;
    this.imageSrc = "";
    this.CovidReportScore = false;
    this.AadharReportRes = false;
    this.PanReportRes = false;
    this.CarnoReportRes = false;
    this.ModelHeaderTxt = true;
    if (e == 0) {
      this.CovidReportDiv = true;
      this.AadharDiv = false;
      this.PanCardDiv = false;
      this.CarnoDiv = false;
      this.PassportDiv = false;
      this.ChequeDiv = false;
      this.DrivingLicDiv = false;
      this.VoterDiv = false;
    }
    if (e == 1) {
      this.CovidReportDiv = false;
      this.AadharDiv = true;
      this.PanCardDiv = false;
      this.CarnoDiv = false;
      this.PassportDiv = false;
      this.ChequeDiv = false;
      this.DrivingLicDiv = false;
      this.VoterDiv = false;
    }
    if (e == 2) {
      this.CovidReportDiv = false;
      this.AadharDiv = false;
      this.PanCardDiv = true;
      this.CarnoDiv = false;
      this.PassportDiv = false;
      this.ChequeDiv = false;
      this.DrivingLicDiv = false;
      this.VoterDiv = false;
    }
    if (e == 3) {
      this.CovidReportDiv = false;
      this.AadharDiv = false;
      this.PanCardDiv = false;
      this.CarnoDiv = true;
      this.PassportDiv = false;
      this.ChequeDiv = false;
      this.DrivingLicDiv = false;
      this.VoterDiv = false;
    }
    if (e == 4) {
      this.CovidReportDiv = false;
      this.AadharDiv = false;
      this.PanCardDiv = false;
      this.CarnoDiv = false;
      this.PassportDiv = true;
      this.ChequeDiv = false;
      this.DrivingLicDiv = false;
      this.VoterDiv = false;
    }
    if (e == 5) {
      this.CovidReportDiv = false;
      this.AadharDiv = false;
      this.PanCardDiv = false;
      this.CarnoDiv = false;
      this.PassportDiv = false;
      this.ChequeDiv = true;
      this.DrivingLicDiv = false;
      this.VoterDiv = false;
    }
    if (e == 6) {
      this.CovidReportDiv = false;
      this.AadharDiv = false;
      this.PanCardDiv = false;
      this.CarnoDiv = false;
      this.PassportDiv = false;
      this.ChequeDiv = false;
      this.DrivingLicDiv = true;
      this.VoterDiv = false;
    }
    if (e == 7) {
      this.CovidReportDiv = false;
      this.AadharDiv = false;
      this.PanCardDiv = false;
      this.CarnoDiv = false;
      this.PassportDiv = false;
      this.ChequeDiv = false;
      this.DrivingLicDiv = false;
      this.VoterDiv = true;
    }


    //     if(e==6){
    //       var dat={};
    //       debugger
    //       this.commonService.FaceMask(dat).subscribe((data: any) => {
    // debugger
    //       });
    //     }
  }

  onFileChangeCheque(event) {
    const reader = new FileReader();
    const [file] = event.target.files;
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imageSrc = reader.result as string;
    }
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.bulkUploadFormCheque.patchValue({
        fileSource: file
      });
    } else {
      alert("upload file !!!")
    }
  }

  onSubmitbulkUploadFormCheque(val) {
    console.log(this.imageClickValue)
    debugger
    if (this.imageClickValue == 5) {
      const formData = new FormData();
      formData.append('file', this.bulkUploadFormCheque.get('fileSource').value);
      this.commonService.Chequenumber(formData).subscribe((data: any) => {
        if (data.Success === "true") {
          this.CovidReportScore = false;
          this.AadharReportRes = false;
          this.PanReportRes = false;
          this.CarnoReportRes = false;
          this.PassportReportRes = false;
          this.ChequeReportRes = true;
          this.ChequeRes = "Cheque Number :" + data.Chequenumber;
          this.ChequeResultJsonRes = JSON.stringify(data);
        }
        else {
          this.CovidReportScore = false;
          this.AadharReportRes = false;
          this.PanReportRes = false;
          this.CarnoReportRes = false;
          this.PassportReportRes = false;
          this.ChequeReportRes = true;
          this.ChequeRes = data.message;
          this.ChequeResultJsonRes = JSON.stringify(data);
        }

      });
    }
    if (this.imageClickValue == 6) {
      const formData = new FormData();
      formData.append('file', this.bulkUploadFormCheque.get('fileSource').value);
      this.commonService.Drivinglicense(formData).subscribe((data: any) => {
        if (data.Success === "true") {
          this.CovidReportScore = false;
          this.AadharReportRes = false;
          this.PanReportRes = false;
          this.CarnoReportRes = false;
          this.PassportReportRes = false;
          this.ChequeReportRes = false;
          this.DrivingLicReportRes=true;
          this.DrivingLicRes = data;
          this.DrivingLicResultJsonRes = JSON.stringify(data);
        }
        else {
          this.CovidReportScore = false;
          this.AadharReportRes = false;
          this.PanReportRes = false;
          this.CarnoReportRes = false;
          this.PassportReportRes = false;
          this.ChequeReportRes = false;
          this.DrivingLicReportRes=true;
          this.DrivingLicRes = data.message;
          this.DrivingLicResultJsonRes = JSON.stringify(data);
        }

      });
    }
    if (this.imageClickValue == 7) {
      const formData = new FormData();
      formData.append('file', this.bulkUploadFormCheque.get('fileSource').value);
      this.commonService.Voterid(formData).subscribe((data: any) => {
        if (data.Success === "true") {
          this.CovidReportScore = false;
          this.AadharReportRes = false;
          this.PanReportRes = false;
          this.CarnoReportRes = false;
          this.PassportReportRes = false;
          this.ChequeReportRes = false;
          this.DrivingLicReportRes=false;
          this.VoterReportRes=true;
          this.VoterRes = data;
          this.VoterResultJsonRes = JSON.stringify(data);
        }
        else {
          this.CovidReportScore = false;
          this.AadharReportRes = false;
          this.PanReportRes = false;
          this.CarnoReportRes = false;
          this.PassportReportRes = false;
          this.ChequeReportRes = false;
          this.DrivingLicReportRes=false;
          this.VoterReportRes=true;
          this.VoterRes = data.message;
          this.VoterResultJsonRes = JSON.stringify(data);
        }

      });
    }
  }

  onFileChangePassport(event) {
    const reader = new FileReader();
    const [file] = event.target.files;
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imageSrc = reader.result as string;
    }
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.bulkUploadFormPassport.patchValue({
        fileSource: file
      });
    } else {
      alert("upload file !!!")
    }
  }

  onSubmitbulkUploadFormPassport(val) {
    console.log(this.imageClickValue)
    debugger
    if (this.imageClickValue == 4) {
      const formData = new FormData();
      formData.append('file', this.bulkUploadFormPassport.get('fileSource').value);
      this.commonService.Passport(formData).subscribe((data: any) => {
        if (data.Success === "true") {
          this.CovidReportScore = false;
          this.AadharReportRes = false;
          this.PanReportRes = false;
          this.CarnoReportRes = false;
          this.PassportReportRes = true;
          debugger
          // this.Res = "Car Number :" + data.Result;
          this.PassportRes = data;
          this.PassportResultJsonRes = JSON.stringify(data);
        }
        else {
          this.CovidReportScore = false;
          this.AadharReportRes = false;
          this.PanReportRes = false;
          this.CarnoReportRes = false;
          this.PassportReportRes = true;
          this.passportRes = data.message;
          this.PassportResultJsonRes = JSON.stringify(data);
        }

      });
    }
  }
  onFileChangeCarno(event) {
    const reader = new FileReader();
    const [file] = event.target.files;
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imageSrc = reader.result as string;
    }
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.bulkUploadFormCarno.patchValue({
        fileSource: file
      });
    } else {
      alert("upload file !!!")
    }
  }

  onSubmitbulkUploadFormCarno(val) {
    console.log(this.imageClickValue)
    debugger
    if (this.imageClickValue == 3) {
      const formData = new FormData();
      formData.append('file', this.bulkUploadFormCarno.get('fileSource').value);
      this.commonService.UploadCarno(formData).subscribe((data: any) => {
        if (data.Success === "True") {
          this.CovidReportScore = false;
          this.AadharReportRes = false;
          this.PanReportRes = false;
          this.CarnoReportRes = true;
          debugger
          this.CarnoRes = "Car Number :" + data.Result;
          this.CarnoResultJsonRes = JSON.stringify(data);
        }
        else {
          this.CovidReportScore = false;
          this.AadharReportRes = false;
          this.PanReportRes = false;
          this.CarnoReportRes = true;
          this.CarnoRes = data.message;
          this.CarnoResultJsonRes = JSON.stringify(data);
        }

      });
    }
  }
  onFileChangePan(event) {
    const reader = new FileReader();
    const [file] = event.target.files;
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imageSrc = reader.result as string;
    }
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.bulkUploadFormPan.patchValue({
        fileSource: file
      });
    } else {
      alert("upload file !!!")
    }
  }

  onSubmitbulkUploadFormPan(val) {
    console.log(this.imageClickValue)
    debugger
    if (this.imageClickValue == 2) {
      const formData = new FormData();
      formData.append('file', this.bulkUploadFormPan.get('fileSource').value);
      this.commonService.UploadPan(formData).subscribe((data: any) => {
        this.CovidReportScore = false;
        this.AadharReportRes = false;
        this.PanReportRes = true;
        this.PanResName = data.Name;
        this.PanResNo = data.PanCardNumber;
        this.PanResDOB = data.DateofBrith;
        this.PanResultJsonRes = JSON.stringify(data);
      });
    }
  }
  onFileChangeAadhar(event) {
    const reader = new FileReader();
    const [file] = event.target.files;
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imageSrc = reader.result as string;
    }
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.bulkUploadFormAadhar.patchValue({
        fileSource: file
      });
    } else {
      alert("upload file !!!")
    }
  }

  onSubmitbulkUploadFormAadhar(val) {
    console.log(this.imageClickValue)
    debugger
    if (this.imageClickValue == 1) {
      const formData = new FormData();
      formData.append('file', this.bulkUploadFormAadhar.get('fileSource').value);
      this.commonService.UploadAadhar(formData).subscribe((data: any) => {
        this.CovidReportScore = false;
        this.AadharReportRes = true;
        this.AadharResName = data.Name;
        this.AadharResNo = data.AadharNumber;
        this.AadharResDOB = data.DateofBirth;
        this.AadharResultJsonRes = JSON.stringify(data);
      });
    }
  }
  onFileChange(event) {
    debugger
    const reader = new FileReader();
    const [file] = event.target.files;
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imageSrc = reader.result as string;
    }
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.bulkUploadForm.patchValue({
        fileSource: file
      });
    } else {
      alert("upload file !!!")
    }
  }

  onSubmitbulkUploadForm(val) {
    debugger
    console.log(this.imageClickValue)
    if (this.imageClickValue == 0) {
      const formData = new FormData();
      formData.append('file', this.bulkUploadForm.get('fileSource').value);
      this.commonService.bulkUpload(formData).subscribe((data: any) => {
        this.CovidReportScore = true;
        this.CovidResult = data.Result;
        this.CovidScore = data.Score;
        this.CovidResultJsonRes = JSON.stringify(data);
      });
    }
  }
}
