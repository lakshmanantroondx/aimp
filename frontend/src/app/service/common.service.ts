import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private http: HttpClient, private router: Router) { }

  bulkUpload(data: any) {
    return this.http.post<any>(`${environment.apiUrl}Covid` , data)
  }
  UploadAadhar(data: any) {
    return this.http.post<any>(`${environment.apiUrl}Aadharcard` , data)
  }

  UploadPan(data: any) {
    return this.http.post<any>(`${environment.apiUrl}Pancard` , data)
  }
  UploadCarno(data: any) {
    return this.http.post<any>(`${environment.apiUrl}Carnoplate` , data)
  }
  FaceMask(data: any) {
    return this.http.post<any>(`${environment.apiUrl}Facemask` , data)
  }
  Passport(data: any) {
    return this.http.post<any>(`${environment.apiUrl}Passport` , data)
  }
  Chequenumber(data: any) {
    return this.http.post<any>(`${environment.apiUrl}Chequenumber` , data)
  }
  Drivinglicense(data: any) {
    return this.http.post<any>(`${environment.apiUrl}Drivinglicense` , data)
  }
  Voterid(data: any) {
    return this.http.post<any>(`${environment.apiUrl}Voterid` , data)
  }
  usercreate(data: any) {
    return this.http.post<any>(`${environment.apiNodeUrl}/usercreate` , data)
  }
  userlogin(data: any) {
    return this.http.post<any>(`${environment.apiNodeUrl}/userlogin` , data)
  }
}
